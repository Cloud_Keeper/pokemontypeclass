# Examples of the Conversation system

from evennia import create_object
from typeclasses import characters_trainers

#####
# Talk command.
#####

class CmdTalk(COMMAND_DEFAULT_CLASS):
    """
    Talk to a NPC. Triggers the corresponding objects at_talk hook.

    Usage:
        talk <obj> =['say message][:pose message][:'say message]
    """
    key = "talk"
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """Talk to a Character."""

        # Set up function variables.
        caller = self.caller
        rplist = self.rhs.split(":") if self.rhs else None

        # Find and confirm suitability of target.
        if not self.args:
            caller.msg("Talk to whom?")
            return

        target = caller.search(self.args,
                               typeclass=Trainer,
                               nofound_string="You cannot talk to {}.".format(obj))
        if not target:
            return

        # Handle roleplay entries.
        if rplist:
            for text in rplist:
                if text[0] in ["'"]:
                    caller.execute_cmd("Say " + text[1:])
                else:
                    caller.execute_cmd("Pose " + text)

        # Call use_object hook on object.
        target.at_talk(caller)

#####
# at_talk hook on Character
#####

    def at_talk(self, caller):
        """
        Called when obj targeted by a talk command.
        """

        def evtable_options_formatter(optionlist, caller=None):
            """
            We don't want any options displayed.
            """
            return ""

        # Allow talk command to work with players.
        if self.has_player:
            caller.execute_cmd("Say Hi %s" % self.key)
            return

        if not self.db.conversation:
            caller.msg("%s appears too busy to talk right now." % self.key)
            return

        conversation = {}
        exec self.db.conversation

        EvMenu(caller, conversation, cmdset_mergetype="Replace",
               startnode="node1", cmdset_priority=1,
               cmd_on_exit="", node_formatter=null_node_formatter,
               options_formatter=evtable_options_formatter)

# Basic NPC Conversation - Sentences one after the other.

npc1 = create_object(characters_trainers.Trainer,
                    key="test", location=caller.location)
npc1.db.desc = "Basic NPC Conversation - Sentences one after the other."
npc1.db.conversation = """
conversation = {"node1": lambda caller: (
                         "One",
                         ({"key": "_default",
                           "goto": "node2"})),
                "node2": lambda caller: (
                         "Two",
                         ({"key": "_default",
                           "goto": "node3"})),
                "node3": lambda caller: (
                         "Three",
                         None)}
"""

# Basic NPC Conversation - Divergant choices.

npc2 = create_object(characters_trainers.Trainer,
                    key="test", location=caller.location)
npc2.db.desc = "Basic NPC Conversation - Divergant choices."
npc2.db.conversation = """
conversation = {"node1": lambda caller: (
                         "Yes or No",
                         ({"key": "yes",
                           "goto": "node2a"},
                          {"key": "no",
                           "goto": "node2b"})),
                "node2a": lambda caller: (
                         "You said Yes.",
                         None),
                "node2b": lambda caller: (
                         "You said No.",
                         None)}
"""

# Advanced NPC Conversation - Conditional messages

npc3 = create_object(characters_trainers.Trainer,
                    key="test", location=caller.location)
npc3.db.desc = "Advanced NPC Conversation - Conditional messages"
npc3.db.conversation = """
global test
test = "You are Superuser" if caller.key == "user" else "You are not Superuser"
conversation = {"node1": lambda caller: (
                         test,
                         None)}
"""

# Advanced NPC Conversation - Node triggers action

npc4 = create_object(characters_trainers.Trainer,
                    key="test", location=caller.location)
npc4.db.desc = "Advanced NPC Conversation - Node triggers action"
npc4.db.conversation = """
def mynode(caller):

   caller.location.msg_contents("Action Triggered.")

   text = caller.key
   options = None
   return text, options

conversation = {"node1": lambda caller: (
                         "Basic Node",
                         ({"key": "_default",
                           "goto": "node2"})),
                "node2": mynode}
"""

# All Node Prototype - with dict

npc4 = create_object(characters_trainers.Trainer,
                    key="test", location=caller.location)
npc4.db.desc = "Advanced NPC Conversation - Node triggers action"
npc4.db.conversation = """
def node1(caller):
    text = "One"
    options = {"key": "_default",
               "goto": "node2"}
    return text, options

def node2(caller):
    text = "Two"
    options = {"key": "_default",
               "goto": "node3"}
    return text, options
   
def node3(caller):
    text = "Three"
    options = None
    return text, options

conversation = {"node1": node1,
                "node2": node2,
                "node3": node3}    
"""

# All Node Prototype - with dict

npc5 = create_object(characters_trainers.Trainer,
                    key="test", location=caller.location)
npc5.db.desc = "All Node Prototype"
npc5.db.conversation = """
def node1(caller):
    text = "One"
    options = {"key": "_default",
               "goto": "node2"}
    return text, options

def node2(caller):
    text = "Two"
    options = {"key": "_default",
               "goto": "node3"}
    return text, options
   
def node3(caller):
    text = "Three"
    options = None
    return text, options

conversation = {"node1": node1,
                "node2": node2,
                "node3": node3}    
"""




















