
# -*- coding: utf-8 -*-

"""
Pallet Town Batchcode
          ▲▲
         Θ☼☼Θ
ΘΘΘΘΘΘΘΘΘΘ☼☼ΘΘΘΘΘΘΘΘ
Θ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓Θ
Θ▓▒▒┌──┐▒▒1▒┌──┐▒▒▓Θ
Θ▓▒▒╞══╡▒▒▒▒╞══╡▒▒▓Θ
Θ▓▒□└■─┘▒▒▒□└■─┘▒▒▓Θ
Θ▓▒▒▒2▒▒▒▒▒▒▒3▒▒▒▒▓Θ
Θ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓Θ
Θ▓▒▒▒▒▒▒▒▒┌────┐▒▒▓Θ
Θ▓▒▒πππ□▒▒╞════╡▒▒▓Θ
Θ▓▒▒▓▓▓▓▒▒│    │▒▒▓Θ
Θ▓▒▒▓▓▓▓▒▒└─■──┘▒▒▓Θ
Θ▓▒▒▒4▒▒▒▒▒▒5▒▒▒▒▒▓Θ
Θ▓▒▒▒▒▒▒▒▒πππ□ππ▒▒▓Θ
Θ▓▓▓≈≈≈≈▒▒▓▓▓▓▓▓▒▒▓Θ
Θ▓▓▓≈6≈≈▒▒▓▓7▓▓▓▒▒▓Θ
Θ▓▓▓≈≈≈≈▒▒▒▒▒▒▒▒▒▒▓Θ
ΘΘ▓▓≈≈≈≈ΘΘΘΘΘΘΘΘΘΘΘΘ
  ▼▼▼▼▼▼

"""

from evennia import create_object
from typeclasses import rooms, exits, objects_computer, characters_trainers

# INITIAL AREA SETUP
pallet_crossroads = create_object(rooms.Room, key="Pallet Town Crossroads")
pallet_home = create_object(rooms.Room, key="Home")
pallet_house = create_object(rooms.Room, key="House")
pallet_riverbank = create_object(rooms.Room, key="Pallet Town River Bank")
pallet_oslab = create_object(rooms.Room, key="Outside Laboratory")
pallet_lab = create_object(rooms.Room, key="Laboratory Atrium")
pallet_oak = create_object(rooms.Room, key="Professor Oak's Office")
pallet_corral = create_object(rooms.Room, key="Laboratory Corral")
pallet_river = create_object(rooms.Room, key="Pallet Town River")
pallet_park = create_object(rooms.Room, key="Pallet Town Park")

#--
pallet_northroad = create_object(rooms.Room, key="Northern Road")
#--

# COMPLETE AREA SETUP

"""
1. Pallet Town Crossroads
              ▲▲
    ΘΘΘΘΘΘΘΘΘΘ☼☼ΘΘΘΘΘΘΘΘ
    Θ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓Θ
    Θ▓▒▒┌──┐▒▒1▒┌──┐▒▒▓Θ
    Θ▓▒▒╞══╡▒▒▒▒╞══╡▒▒▓Θ
    Θ▓▒□└■─┘▒▒▒□└■─┘▒▒▓Θ
    Θ▓▒▒▒2▒▒▒▒▒▒▒3▒▒▒▒▓Θ
    Θ▓▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▓Θ
    Θ▓▒▒▒▒▒▒▒▒┌────┐▒▒▓Θ
    Θ▓▒▒πππ□▒▒╞════╡▒▒▓Θ
            ▼▼

"""
# ROOM DETAILS
pallet_crossroads.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
crossroads_ex1 = create_object(exits.Exit, key="Enter Home", 
                               aliases=["home"],
                               location=pallet_crossroads,
                               destination=pallet_home)
crossroads_ex2 = create_object(exits.Exit, key="South",
                               aliases=["s", "south"],
                               location=pallet_crossroads,
                               destination=pallet_riverbank)
crossroads_ex3 = create_object(exits.Exit, key="Enter House",
                               aliases=["house"],
                               location=pallet_crossroads,
                               destination=pallet_house)
crossroads_ex4 = create_object(exits.PalletTownExit, key="North",
                               aliases=["n", "north"],
                               location=pallet_crossroads,
                               destination=pallet_northroad)

# NPCS/OBJECTS

"""
2. Pallet Town Home
    Θ▓▒▒▒┌──┐▒
    Θ▓▒▒▒╞══╡▒
    Θ▓▒▒□└■─┘▒
    Θ▓▒▒▒▒2▒▒▒

"""
# ROOM DETAILS
pallet_home.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
home_ex1 = create_object(exits.Exit, key="Exit", aliases=["e", "exit"],
                         location=pallet_home,
                         destination=pallet_crossroads)

# NPCS/OBJECTS
pallet_home_mum = create_object(characters_trainers.Trainer,
                                key="Mum", aliases=["Mom", "mom", "mum"],
                                location=pallet_home)
pallet_home_mum.db.desc = """\
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
"""
pallet_home_mum.db.conversation = """
def node1(caller):
    if not caller.party.list:
        text = ("Right. All boys leave home some day. It said so on TV.\\n"
                "PROF.OAK, next door, is looking for you.")
        return utils_text.nobordertext(text, "Mum"), None

    text = "%s! You should take a quick rest." % caller.key
    options = {"key": "_default",
               "goto": "node2"}
    return utils_text.nobordertext(text, "Mum"), options

def node2(caller):
    text = "..."
    options = {"key": "_default",
               "goto": "node3"}
    return utils_text.floatingtext(text, "Mum"), options

def node3(caller):
    for pokemon in caller.party:
        pokemon.health.fill()
    text = "Oh good! You and your POKEMON are looking great! Take care now!"
    options = None
    return utils_text.nobordertext(text, "Mum"), options

conversation = {"node1": node1,
                "node2": node2,
                "node3": node3}
"""

"""
3. Pallet Town House
    ▒▒┌──┐▒▒▒▒▓Θ
    ▒▒╞══╡▒▒▒▒▓Θ
    ▒□└■─┘▒▒▒▒▓Θ
    ▒▒▒3▒▒▒▒▒▒▓Θ

"""
# ROOM DETAILS
pallet_house.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
house_ex1 = create_object(exits.Exit, key="Exit", aliases=["e", "exit"],
                          location=pallet_house,
                          destination=pallet_crossroads)

# NPCS/OBJECTS
pallet_house_girl = create_object(characters_trainers.Trainer,
                                  key="Young Girl", aliases=["girl"],
                                  location=pallet_house)
pallet_house_girl.db.desc = """\
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
"""
pallet_house_girl.db.conversation = """
def node1(caller):
    text = "Hi %s!" % caller.key
    options = None
    return utils_text.nobordertext(text, "Girl"), options

conversation = {"node1": node1}
"""

"""
4. Pallet Town River Bank
    Θ▓▒▒πππ□▲▲╞
    Θ▓▒▒▓▓▓▓▒▒│
    Θ▓▒▒▓▓▓▓▒▒└
    Θ▓▒▒▒4▒▒▒▒►
    Θ▓▒▒▒▒▒▒▒▒π
    Θ▓▓▓≈≈≈≈
    Θ▓▓▓≈6≈≈
    Θ▓▓▓≈≈≈≈
    ΘΘ▓▓≈≈≈≈
      ▼▼▼▼▼▼

"""
# ROOM DETAILS
pallet_riverbank.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
riverbank_ex1 = create_object(exits.Exit, key="North", aliases=["n", "north"],
                              location=pallet_riverbank,
                              destination=pallet_crossroads)
riverbank_ex2 = create_object(exits.Exit, key="East", aliases=["e", "east"],
                              location=pallet_riverbank,
                              destination=pallet_oslab)

# NPCS/OBJECTS
pallet_riverbank_girl = create_object(characters_trainers.Trainer,
                                      key="Young Girl", aliases=["Girl"],
                                      location=pallet_riverbank)
pallet_riverbank_girl.db.desc = """\
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
"""
pallet_riverbank_girl.db.conversation = """
def node1(caller):
    text = "I'm raising POKEMON too! When they get strong, they can protect me!"
    options = None
    return utils_text.nobordertext(text, "Girl"), options

conversation = {"node1": node1}
"""

"""
5. Oak's Laboratory
    ▒┌────┐▒▒▓Θ
    ▒╞════╡▒▒▓Θ
    ▒│    │▒▒▓Θ
    ▒└─■──┘▒▒▓Θ
    ◄▒▒5▒▒▒▒▒▓Θ
    ▒πππ□ππ▼▼▓Θ
"""
# ROOM DETAILS
pallet_oslab.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
oslab_ex1 = create_object(exits.Exit, key="West", aliases=["w", "west"],
                          location=pallet_oslab,
                          destination=pallet_riverbank)
oslab_ex2 = create_object(exits.Exit, key="South", aliases=["s", "south"],
                          location=pallet_oslab,
                          destination=pallet_park)
oslab_ex3 = create_object(exits.Exit, key="Enter", aliases=["e", "enter"],
                          location=pallet_oslab,
                          destination=pallet_lab)

# NPCS/OBJECTS
pallet_oslab_boy = create_object(characters_trainers.Trainer,
                                 key="Young Boy", aliases=["Boy"],
                                 location=pallet_oslab)
pallet_oslab_boy.db.desc = """\
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
"""
pallet_oslab_boy.db.conversation = """
def node1(caller):
    text = "Technology is incredible! You can now store and recall items and POKEMON as data via PC!"
    options = None
    return utils_text.nobordertext(text, "Boy"), options

conversation = {"node1": node1}
"""

# ROOM DETAILS
pallet_lab.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
lab_ex1 = create_object(exits.Exit, key="Enter", aliases=["enter"],
                        location=pallet_lab,
                        destination=pallet_oak)
lab_ex2 = create_object(exits.Exit, key="Exit", aliases=["exit"],
                        location=pallet_lab,
                        destination=pallet_oslab)

# NPCS/OBJECTS
pallet_lab_aide = create_object(characters_trainers.Trainer,
                                 key="Lab Aide", aliases=["Aide"],
                                 location=pallet_lab)
pallet_lab_aide.db.desc = """\
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
"""
pallet_lab_aide.db.conversation = """
def node1(caller):
    text = "I study POKEMON as PROF.OAK'S AIDE."
    options = None
    return utils_text.nobordertext(text, "Lab Aide"), options

conversation = {"node1": node1}
"""

pallet_lab_girl = create_object(characters_trainers.Trainer,
                                 key="Young Girl", aliases=["Girl"],
                                 location=pallet_lab)
pallet_lab_girl.db.desc = """\
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
"""
pallet_lab_girl.db.conversation = """
def node1(caller):
    text = "PROF.OAK is the authority on POKEMON! Many POKEMON trainers hold him in high regard!"
    options = None
    return utils_text.nobordertext(text, "Girl"), options

conversation = {"node1": node1}
"""

# ROOM DETAILS
pallet_oak.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
oak_ex1 = create_object(exits.Exit, key="Enter", aliases=["enter"],
                        location=pallet_oak,
                        destination=pallet_corral)
oak_ex2 = create_object(exits.Exit, key="Exit", aliases=["exit"],
                        location=pallet_oak,
                        destination=pallet_lab)

# NPCS/OBJECTS
pallet_oak_oak = create_object(characters_trainers.Trainer,
                                 key="Professor Oak", aliases=["Prof", "Oak"],
                                 location=pallet_oak)
pallet_oak_oak.locks.add("view:attr(party)")
pallet_oak_oak.db.desc = """\
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
NPC Description NPC Description NPC Description NPC Description NPC Description
"""
pallet_oak_oak.db.conversation = """
def node1(caller):
    text = "%s, raise your young POKEMON by making it fight!" % caller.key
    options = None
    return utils_text.nobordertext(text, "Prof Oak"), options

conversation = {"node1": node1}
"""

oak_computer = create_object(objects_computer.Computer, key="Computer",
                             aliases=["cpu"], location=pallet_oak)
oak_computer.db.desc = """\
There's an e-mail message here! ... Calling all POKEMON trainers!
The elite trainers of POKEMON LEAGUE are ready to take on all
comers! Bring your best POKEMON and see how you rate as a trainer!

POKEMON LEAGUE HQ INDIGO PLATEAU
PS: PROF.OAK, please visit us! ...\
                       """
# ROOM DETAILS
pallet_corral.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
corral_ex1 = create_object(exits.Exit, key="Exit", aliases=["exit"],
                           location=pallet_corral,
                           destination=pallet_oak)

# NPCS/OBJECTS
                           
"""
6. Pallet Town Park
    ▒▒▒πππ□ππ▲▲▓Θ
    ≈▒▒▓▓▓▓▓▓▒▒▓Θ
    ≈▒▒▓▓7▓▓▓▒▒▓Θ
    ≈▒▒▒▒▒▒▒▒▒▒▓Θ
    ≈ΘΘΘΘΘΘΘΘΘΘΘΘ
"""
# ROOM DETAILS
pallet_park.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
park_ex1 = create_object(exits.Exit, key="North", aliases=["n", "north"],
                         location=pallet_park,
                         destination=pallet_oslab)

# NPCS/OBJECTS

"""
X. Northern Road
         Θ☼☼Θ
         Θ☼☼Θ
         ▒▒▒▒
"""
# ROOM DETAILS
pallet_northroad.db.desc = """\
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
Room Description Room Description Room Description Room Description Room
Description Room Description Room Description Room Description Room Description
"""

# EXITS
road_ex1 = create_object(exits.Exit, key="South", aliases=["s", "south"],
                         location=pallet_northroad,
                         destination=pallet_crossroads)

# NPCS/OBJECTS
