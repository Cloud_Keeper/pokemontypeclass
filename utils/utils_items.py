"""
FILL IN LATER
"""

from world import pokedex, rules
from random import randint, uniform
from typeclasses.objects_pokemon import Pokemon
from evennia.utils.create import create_object
from world.itemlist import ITEMLIST
from typeclasses.objects_items import Pokeball


def create_item(key, number=1, typeclass=None, location=None, home=None,
                permissions=None, locks=None, aliases=None, tags=None,
                destination=None, report_to=None, nohome=False, **kwargs):
    """
    if key in item list then get those details

    create item

    add kwargs as attributes

    return single object or list if multiple.
    """
    key = key.title()

    # Check if standard item and adopt arguments.
    if key in dict(ITEMLIST):
        attributes = ITEMLIST[key].copy()
        permissions = attributes.pop("permissions") if "permissions" in attributes else permissions
        locks = attributes.pop("locks") if "locks" in attributes else locks
        aliases = attributes.pop("aliases") if "aliases" in attributes else aliases
        tags = attributes.pop("tags") if "tags" in attributes else tags
        typeclass = attributes.pop("typeclass")
        kwargs = attributes

    objects = []
    for i in xrange(number):
        # Create object.
        obj = create_object(typeclass, key, location, home,
                            permissions, locks, aliases, tags,
                            destination, report_to, nohome)

        # Add attributes.
        for k, v in kwargs.iteritems():
            obj.attributes.add(k, v)
        objects.append(obj)

    objects = objects[0] if len(objects) == 1 else objects
    return objects
