# -*- coding: utf-8 -*-

"""
PokeText

The module holds convenience functions to return text inside the variously
formatted evTables and evMenus utilised by PokeMud.

"""

from evennia.utils import evtable


def bordertext(text, name=""):
    """
    Takes a string and returns the string formatted in a evTable. Eg.
    
    ?------------------------------------------------------------------------?
        Oak: Hello there! Welcome to the world of POKEMON! My name is OAK!
             People call me the POKEMON PROF!
    ?------------------------------------------------------------------------?
    
    """
    # Formatting values
    if len(name) >= 1:
        name += ":"
    
    padding = 4
    col0_width = len(name) + padding + 2
    col1_width = 79 - col0_width
    
    table = evtable.EvTable(name, text, header=False, border="table",
                            width=80, pad_width=1)
    table.reformat(corner_char="◓", border_char="═", border_left_char=" ",
                   border_right_char=" ")
    table.reformat_column(0, pad_left=padding, width=col0_width, valign="t")
    table.reformat_column(1, pad_right=padding, width=col1_width)
    return table


def nobordertext(text, name=""):
    """
    Takes a string and returns the string formatted in a evTable. Eg.

        Oak: Hello there! Welcome to the world of POKEMON! My name is OAK!
             People call me the POKEMON PROF!

    """
    # Formatting values
    if len(name) >= 1:
        name += ":"

    padding = 4
    col0_width = len(name) + padding + 2
    col1_width = 79 - col0_width

    table = evtable.EvTable(name, text, header=False, border=None,
                            width=80, pad_width=0, pad_top=0)
    table.reformat_column(0, pad_left=padding, width=col0_width, valign="t")
    table.reformat_column(1, pad_right=padding, width=col1_width)
    return table

def floatingtext(text, name=""):
    """
    Takes a string and returns the string formatted in a evTable. Eg.

             Hello there! Welcome to the world of POKEMON! My name is OAK!
             People call me the POKEMON PROF!

    """
    # Formatting values
    if len(name) >= 1:
        name = " " * (len(name)+1)

    padding = 4
    col0_width = len(name) + padding + 2
    col1_width = 79 - col0_width

    table = evtable.EvTable(name, text, header=False, border=None,
                            width=80, pad_width=0, pad_top=0)
    table.reformat_column(0, pad_left=padding, width=col0_width, valign="t")
    table.reformat_column(1, pad_right=padding, width=col1_width)
    return table
