"""
FILL IN LATER
"""

from world import pokedex, rules
from random import randint, uniform
from typeclasses.objects_pokemon import Pokemon
from evennia.utils.create import create_object


def create_pokemon(name, location=None, level=1, iv=None, ev=None, owner=None,
                   trainer=None, gender=None, pokeball="Pokeball"):
    
    # Create Pokemon.
    pokemon = create_object(typeclass=Pokemon, key=name.title(),
                            location=location)

    # Set Argument values.
    pokemon.db.level = level
    pokemon.db.iv = iv
    pokemon.db.ev = ev
    pokemon.db.gender = gender
    pokemon.db.pokeball = pokeball
            
    try:
        # Conduct sanity checks.
        stat_check(pokemon, rebuild=True)
    except AttributeError as err:
        # Delete faulty pokemon and explain why.
        pokemon.delete()
        raise AttributeError(err)

    # Retrieve pokedex values.
    pokemon.db.desc = pokemon.pokedex("description")

    # Set up equipment handler values.
    pokemon.db.limbs = pokemon.pokedex("body")
    slots = {}
    for limb in pokemon.db.limbs:
        for slot in limb[1]:
            slots[slot] = None
    pokemon.db.slots = slots

    # Call health handler
    pokemon.health.fill()

    # Call experience handler & move handler
    pokemon.exp.set_level(pokemon.db.level)

    # Set up ownership
    pokemon.db.trainer = trainer
    pokemon.db.owner = [owner]

    return pokemon
                    
            
def stat_check(pokemon, rebuild=False):
    """
    To be called whenever stats are to be determined/checked. Will
    replace bad or insane values with appropriate ones and recaculate
    stats.
    
    If rebuild true it will alter pokemon to make sure it conforms.

    TODO rebuild option. If not selected it raises an error.
    """
    
    # Check if Pokemon.
    if not isinstance(pokemon, Pokemon):
        string = "Command aborted!\n"
        string += "Not provided object of Pokemon typeclass."
        raise AttributeError(string)
    
    # Check if valid key.
    if pokemon.key not in pokedex.POKEDEX:
        string = "Command aborted!\n"
        string += "Pokemon: '%s' not found.\nPokemon.key must be the name "
        string += "of a valid Pokemon."
        raise AttributeError(string % pokemon.key)

    # Check if level exists, if valid value and in valid range.
    if not pokemon.attributes.has("level"):
        pokemon.db.level = 1
        
    if not isinstance(pokemon.db.level, (int, long)):
        pokemon.db.level = 1
    
    if pokemon.db.level < 1:
        pokemon.db.level = 1
    if pokemon.db.level > 100:
        pokemon.db.level = 100
    
    # EXP CHECKS
    
    # Check if iv exists, is list, is correct length and valid values
    if not pokemon.attributes.has("iv"):
        pokemon.db.iv = [randint(0, 15), randint(0, 15), randint(0, 15),
                         randint(0, 15), randint(0, 15), randint(0, 15)]
    
    if not isinstance(pokemon.db.iv, (list, tuple)):
        pokemon.db.iv = [randint(0, 15), randint(0, 15), randint(0, 15),
                         randint(0, 15), randint(0, 15), randint(0, 15)]
    
    if not len(pokemon.db.iv) == 6:
        if len(pokemon.db.iv) < 6:
            for i in range(6-len(pokemon.db.iv)):
                pokemon.db.iv.append(randint(0, 15))
        if len(pokemon.db.iv) > 6:
            for i in range(-6+len(pokemon.db.iv)):
                del pokemon.db.iv[-1]
    
    for i in range(len(pokemon.db.iv)):
        if not isinstance(pokemon.db.iv[i], (int, long)):
            pokemon.db.iv[i] = randint(0, 15)
        if i < 0 or i > 15:
            pokemon.db.iv[i] = randint(0, 15)

    # Check if ev exists, is list, is correct length and valid values
    if not pokemon.attributes.has("ev"):
        pokemon.db.ev = [0, 0, 0, 0, 0, 0]
    
    if not isinstance(pokemon.db.ev, (list, tuple)):
        pokemon.db.ev = [0, 0, 0, 0, 0, 0]
    
    if not len(pokemon.db.ev) == 6:
        if len(pokemon.db.ev) < 6:
            for i in range(6-len(pokemon.db.ev)):
                pokemon.db.ev.append(0)
        if len(pokemon.db.ev) > 6:
            for i in range(-6+len(pokemon.db.ev)):
                del pokemon.db.ev[-1]
    
    for i in range(len(pokemon.db.ev)):
        if not isinstance(pokemon.db.ev[i], (int, long)):
            pokemon.db.ev[i] = 0
        if i < 0 or i > 65535:
            if pokemon.db.ev < 1:
                pokemon.db.ev = 0
            if pokemon.db.ev > 65535:
                pokemon.db.ev = 65535
               
    # GENDER CHECKS
    if not pokemon.attributes.has("gender") or pokemon.db.gender not in ("Male", "Female", "Unknown"):
        gender = pokemon.pokedex("gender_ratio")
        if isinstance(gender, str):
            pokemon.db.gender = gender
        else:
            if round(uniform(0, 100), 1) <= gender:
                pokemon.db.gender = "Male"
            else:
                pokemon.db.gender = "Female"
