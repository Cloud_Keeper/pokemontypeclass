"""
The catalogue holds all hardcoded values for items.

"""

MOVELIST = {

    "Tackle": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Growl": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Leech Seed": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Vine Whip": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Poison Powder": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Sleep Powder": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Razor Leaf": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Sweet Scent": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Growth": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "Synthesis": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
    "SolarBeam": {
        "Power": 35,
        "Accuracy": 95,
        "PP": 35,
        "desc": "A full-body charge attack.",
    },
}
