
# -*- coding: utf-8 -*-

"""
Test Batchcode
"""

from evennia import create_object
from typeclasses import rooms, exits, objects_computer, characters_trainers


attendant = create_object(characters_trainers.Trainer, key="Attendant",
                          location=caller.location)
attendant.db.desc = """\
A young male shop attendant.
"""

attendant.db.stock = [
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
]

attendant.db.conversation = """
def node1(caller):
    from world import shopmenu
    from evennia.utils.evmenu import EvMenu

    EvMenu(caller, "world.shopmenu", startnode="start_node",
       cmd_on_exit="look", node_formatter=shopmenu.node_formatter,
       options_formatter=shopmenu.options_formatter,
       display_message="May I help you?", page=0,
       npc=caller.ndb._menutree.npc)

    return None, None

conversation = {"node1": node1}
"""


# from evennia.utils import evtable
# from evennia.utils.evform import EvForm
#
# string1 = """
# ◓═══════════════════════════════◓══MONEY══◓
# ║ xAxxxxxxxxxxxxxxxxxxxxxxxxxxx ║ xBxxxxx ║
# ║ xxxxxxxxxxxxxxxxxxxxxxxxxxxxx ◓═════════◓
# """
# form1 = EvForm(form={"FORM": string1},
#                cells={"A": "Masterball",
#                       "B": str(caller.db.money)})
#
#
# tablestr = ('This appears to be a rather old short sword.The hilt is wound '
#             'with red leather and its end is shaped like an owl sitting on a'
#             ' shield. On the blade itself the words "Royal Ankhe-Morepork '
#             'Citie Watch & Ward" are engraved on one side and on the other '
#             '"FABRICATI DIEM, PVNCTI AGVNT CELERITER"')
# table = evtable.EvTable(tablestr, width=43,
#                         border_right_char="║", border_left_char="║",
#                         border_top=0, border_bottom=0)
#
#
# string2 = """\n
# ◓═════════════════════════════════════════◓
# ║  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
# ║  xAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
# ║  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
# ◓═════════════════════════════════════════◓
# """
# form2 = EvForm(form={"FORM": string2},
#                cells={"A": ('I can pay you x for that.\n'
#                             'How many would you like to sell?')})
#
#
#
#
# caller.msg(str(form1) + str(table) + str(form2))

# # Sort character contents into list of lists of items.
# # inventory = [[item1, item1], [item2, item2],...]
# inventory = []
# for item in caller.contents:
#     match_found = False
#     for i in xrange(len(inventory)):
#         # We will allow for items having same key buy different desc.
#         if item.key == inventory[i][0].key and \
#                         item.db.desc == inventory[i][0].db.desc:
#             inventory[i].append(item)
#             match_found = True
#             break
#     if not match_found:
#         inventory.append([item])
#
# caller.msg(str(inventory))