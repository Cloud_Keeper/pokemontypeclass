
# -*- coding: utf-8 -*-

"""
Scripted Events.

This file holds the scripted events that may be triggered through out the game.

"""

from evennia import utils
from utils import utils_text, utils_pokemon
from world.pokedex import POKEDEX
from evennia.utils import evform
from evennia.utils.evmenu import EvMenu, null_node_formatter, dedent_nodetext_formatter


def evmenu_null_options_formatter(optionlist, caller=None):
    """
    We don't want any options displayed.
    """
    return ""

def evmenu_null_node_formatter(nodetext, optionstext, caller=None):
    """
    A minimalistic node formatter, no lines, frames or extra lines.
    """
    return nodetext

def starting_selection_event(self, caller, iteration=0):
    """
    
    """
    if iteration == 0:
        msg = "Hey! Wait! Don't go out!"
        caller.location.msg_contents(utils_text.nobordertext(msg, "Prof Oak"))
        utils.delay(2, starting_selection_event, self, caller, 1)
    
    elif iteration == 1:
        location = caller.location.key
        msg = "Professor Oak runs into " + location + " and approaches %s."
        caller.msg(msg % "you")
        caller.location.msg_contents(msg % caller.name, exclude=caller)
        utils.delay(3, starting_selection_event, self, caller, 2)
    
    elif iteration == 2:
        msg = "It's unsafe! Wild POKEMON live in tall grass! You need your own POKEMON for your protection."
        caller.location.msg_contents(utils_text.nobordertext(msg, "Prof Oak"))
        utils.delay(3, starting_selection_event, self, caller, 3)
    
    elif iteration == 3:
        msg = "I know! Here, come with me!"
        caller.location.msg_contents(utils_text.floatingtext(msg, "Prof Oak"))
        utils.delay(2, starting_selection_event, self, caller, 4)
    
    elif iteration == 4:
        msg = ("%s leaving " + caller.location.key +
               " following Professor Oak.")
        caller.msg(msg % "You are")
        caller.location.msg_contents(msg % (caller.name + " is"),
                                      exclude=caller)
        caller.location = None
        utils.delay(3, starting_selection_event, self, caller, 5)
        
    elif iteration == 5:
        caller.msg(utils_text.floatingtext(". . .", "Prof Oak"))
        utils.delay(2, starting_selection_event, self, caller, 6)
        
    elif iteration == 6:
        caller.msg("You follow Professor Oak into his office.")
        utils.delay(2, starting_selection_event, self, caller, 7)

    elif iteration == 7:
        caller.location = caller.search("Laboratory",
                                          global_search=True)
        caller.execute_cmd("look")
        utils.delay(2, starting_selection_event, self, caller, 8)
        
    elif iteration == 8:
        EvMenu(caller, "world.events_testarea", cmdset_mergetype="Replace",
               startnode="starting_selection_node", cmdset_priority=1,
               cmd_on_exit="", node_formatter=evmenu_null_node_formatter,
               nodetext_formatter=dedent_nodetext_formatter,
               options_formatter=evmenu_null_node_formatter)


def starting_selection_node(caller):
    text = """\
                             _________
                            /1◓ 2◓ 3◓\\
                            ━┳━━┳━\
"""
    text2 = ("Here, %s! There are 3 POKEMON here! Haha!\n"
             "They are inside the POKE BALLS.\n"
             "When I was young, I was a serious POKEMON trainer.\n"
             "In my old age, I have only 3 left, but you can have one!\n"
             "Choose!" % caller.key)
    text += "\n" + str(utils_text.nobordertext(text2, "Prof Oak"))

    options = ({"key": ("1", "Bulbasaur"),
                "goto": "starting_selection_node2"},
               {"key": ("2", "Charmander"),
                "goto": "starting_selection_node2"},
               {"key": ("3", "Squirtle"),
                "goto": "starting_selection_node2"})
    return text, options


def starting_selection_node2(caller, raw_string):
    text = {"FORM": """
                ┏━━━━━━━━━━━━━━━━━━━━━━━┓
                ┃  No. xAx      xxxxxBxxxxxxxxxxxxxxxxxxx  ┃
                ┃               xxxxxCxxxxxxxxxxxxxxxxxxx  ┃
                ┃               HT xxDxxxxxxxxxxxxxxxxxxx  ┃
                ┃               WT xxExxxxxxxxxxxxxxxxxxx  ┃
                ┣━━━━━━━━━━━━━━━━━━━━━━━┫
                ┃  xxxxxxxxxxxxxxxxxxFxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┗━━━━━━━━━━━━━━━━━━━━━━━┛\
            """}

    poke = ["Bulbasaur", "Charmander", "Squirtle"]
    type = ["plant", "fire", "water"]
    choice = poke[int(raw_string)-1]
    caller.ndb._menutree.choice = choice

    # Build EvForm from values.
    form = evform.EvForm(form=text,
                         cells={"A": POKEDEX[choice].get("index", None),
                                "B": choice,
                                "C": POKEDEX[choice].get("classification", None),
                                "D": POKEDEX[choice].get("height", None),
                                "E": POKEDEX[choice].get("weight", None),
                                "F": POKEDEX[choice].get("entries", None)[0]})
    text = "So! You want the %s POKEMON, %s" % (type[int(raw_string)-1], choice)
    text = str(form) + "\n" + str(utils_text.nobordertext(text, "Prof Oak"))

    options = ({"key": ("n", "no"),
                "goto": "starting_selection_node3"},
               {"key": ("y", "yes"),
                "goto": "starting_selection_node4"})

    return text, options


def starting_selection_node3(caller):
    text = """\
                                 _________
                                /1◓ 2◓ 3◓\\
                                ━┳━━┳━\
    """
    text2 = ("Now, %s, which POKEMON do you want?" % caller.key)
    text += "\n" + str(utils_text.nobordertext(text2, "Prof Oak"))

    options = ({"key": "1",
                "goto": "starting_selection_node2"},
               {"key": "2",
                "goto": "starting_selection_node2"},
               {"key": "3",
                "goto": "starting_selection_node2"})
    return text, options


def starting_selection_node4(caller):
    pokemon = utils_pokemon.create_pokemon(caller.ndb._menutree.choice,
                                           level=5, owner=caller,
                                           trainer=caller)
    caller.party.add(pokemon)
    caller.msg(utils_text.floatingtext(
        "🎵You received a %s🎵" % caller.ndb._menutree.choice, "Prof Oak"))
    msg = ("If a wild POKEMON appears, your POKEMON can fight against it!\n"
           "Now go and start your POKEMON adventure!")
    text = utils_text.nobordertext(msg, "Prof Oak")
    return text, None
