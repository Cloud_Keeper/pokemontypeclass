"""
The catalogue holds all hardcoded values for items.

"""

ITEMLIST = {

    "Pokeball": {
        "typeclass": "typeclasses.objects_items.Pokeball",
        "desc": "A tool for catching wild Pokemon.",
        "ldesc": "Ball",
        "price": 200,
        "bonus": 1.0,
    },
    "Greatball": {
        "typeclass": "typeclasses.objects_items.Pokeball",
        "desc": "A Ball for catching wild Pokemon. More effective than a PokeBall.",
        "ldesc": "Ball",
        "price": 600,
        "bonus": 1.5,
    },
    "Ultraball": {
        "typeclass": "typeclasses.objects_items.Pokeball",
        "desc": "A Ball for catching wild Pokemon. More effective than a Great Ball.",
        "ldesc": "Ball",
        "price": 1200,
        "bonus": 2.0,
    },
    "Masterball": {
        "typeclass": "typeclasses.objects_items.Pokeball",
        "desc": "A Ball that captures any wild Pokemon without fail.",
        "ldesc": "Ball",
        "price": 0,
        "bonus": 255.0,
    },

}
