
# -*- coding: utf-8 -*-

"""
TO START:
    EvMenu(caller, "world.shopmenu", startnode="start_node",
       cmd_on_exit="look", node_formatter=evmenu_null_node_formatter,
       options_formatter=evtable_options_formatter,
       display_message="May I help you?", page=0,
       npc=caller.ndb._menutree.npc)

         ->   Start Node
        |         |
        |     [Buy/Sell]
        |         v
    -> Back> Choice Node
    |   |         |
   Good |         v
    |   |-Confirmation Node  <-
    |            |            |
    |            v            Bad
    |-------Transaction-------|
"""
from typeclasses.objects import Object
from evennia.utils.evmenu import EvMenu, null_node_formatter
from evennia.utils.evtable import EvTable
from evennia.utils.evform import EvForm
from world.itemlist import ITEMLIST
from utils.utils_items import create_item

def node_formatter(nodetext, optionstext, caller=None):
    """
    A minimalistic node formatter, no lines, frames or extra lines.
    """
    return nodetext


def options_formatter(optionlist, caller=None):
    """
    We don't want any options displayed.
    """
    return ""

def start_node(caller):
    """Simply provides users with the shop options."""
    string = """
                ◓═══════════◓                   ◓══MONEY══◓
                ║ 1. BUY    ║                   ║ xAxxxxx ║
                ║ 2. SELL   ║                   ◓═════════◓
                ║ 3. QUIT   ║                   
                ◓═══════════◓
                
                ◓═════════════════════════════════════════◓
                ║  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
                ║  xBxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
                ║  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
                ◓═════════════════════════════════════════◓
    """
    caller.ndb._menutree.page = 0

    form = EvForm(form={"FORM": string},
                  cells={"A": str(caller.db.money),
                         "B": caller.ndb._menutree.display_message})

    options = (
        {"key": ("1", "buy", "b"),
         "goto": "choice_node",
         "exec": lambda caller: setattr(caller.ndb._menutree,
                                        "display_message", "BUY")},

        {"key": ("2", "sell", "s"),
         "goto": "choice_node",
         "exec": lambda caller: setattr(caller.ndb._menutree,
                                        "display_message", "SELL")},

        {"key": ("3", "quit", "q", "exit", "e"),
         "goto": "quit_node"})

    return unicode(form), options


def choice_node(caller):
    """
    Displays a list of available items to buy or sell.

    Available items will be in the following formats:
        If buying - Will draw from npc.db.stock attribute:
        stock = [{key: "Item"}, # Normal item, details in master itemlist
                {key: "Item", desc: "Desc"...}] # Custom item, details in dict.
        If Selling - WIll draw from callers inventory:
        stock = [[item1, item1], [item2, item2],...]
    """
    string = """          
                ◓═══════════◓                   ◓══MONEY══◓
                ║ xAxBUY    ║                   ║ xCxxxxx ║
                ║ xBxSE◓══════════════════════════════════◓
                ║ 3. QU║ x1xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ◓══════║ x2xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                       ║ x3xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ◓══════║ x4xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║      ║ x5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║      ║ x6xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║      ║ xDxxxxxx      BACK      xExxxxxx ║
                ◓══════◓══════════════════════════════════◓
    """
    # Remove any previously stored values:
    caller.ndb._menutree.choice_values = None

    # Set Stock list.
    buying = caller.ndb._menutree.display_message == "BUY"
    if buying:
        caller.ndb._menutree.stock = caller.ndb._menutree.npc.db.stock
    else:
        # Sort character inventory to: [[Potion, Potion], [Repel, Repel],...]
        inventory = []
        for item in caller.contents:
            match_found = False
            for i in xrange(len(inventory)):
                # We will allow for items having same key buy different desc.
                if item.key == inventory[i][0].key and \
                                item.db.desc == inventory[i][0].db.desc:
                    inventory[i].append(item)
                    match_found = True
                    break
            if not match_found:
                inventory.append([item])
        caller.ndb._menutree.stock = inventory
    stock = caller.ndb._menutree.stock

    # Handle Table Values.
    page = caller.ndb._menutree.page

    cells = {}
    cells["A"] = "►  " if buying else "1. "
    cells["B"] = "2. " if buying else "►  "
    cells["C"] = str(caller.db.money)
    cells["D"] = "<PREV PG" if page > 0 else ""
    # TODO v Doesn't work for some reason v
    cells["E"] = "NEXT PG>"

    # Fill individual lines with position, item key and price/number
    pos = 1
    for i in xrange(page, page + 6):
        if i > len(stock) - 1:
            break

        # Line should look like: "12. Pokeball              $200 "
        number = str(i + 1) + "." + ("  " if i + 1 <= 9 else " ")
        key = stock[i]["key"] if buying else stock[i][0].key
        value = None
        if buying:
            value = stock[i].get("price")
            value = ITEMLIST[key]["price"] if not value else value
        else:
            value = "x" + str(len(stock[i]))

        line = " "*32
        key = (key if len(key) <= 21 else key[19:]+"..")
        line = number + key + line[len(number+key):]
        line = line[:27] + str(value)

        cells[pos] = line
        pos += 1

    form = EvForm(form={"FORM": string},
                  cells=cells)

    # Handle Mandatory Options
    options = [{"key": "_default",
                "exec": _check_choice},

               {"key": ("back", "b", "exit", "e"),
                "goto": "start_node",
                "exec": lambda caller: setattr(caller.ndb._menutree,
                                               "display_message",
                                               "Is there anything else I can do?")}]

    # Optional Options: Only allow next page/prev page options if applicable.
    if page > 0:
        options.append({"key": ("previous page", "previous", "prev", "p"),
                        "goto": "choice_node",
                        "exec": lambda caller: setattr(caller.ndb._menutree,
                                                       "page",
                                                       (caller.ndb._menutree.page - 6) if (caller.ndb._menutree.page - 6) >= 0 else 0)})

    if len(stock) > page + 6:
        options.append({"key": ("next page", "next", "n"),
                        "goto": "choice_node",
                        "exec": lambda caller: setattr(caller.ndb._menutree,
                                                       "page",
                                                       caller.ndb._menutree.page + 6)})

    return unicode(form), options


def _check_choice(caller, raw_string):
    """Move to appropriate node based on input."""

    stock = caller.ndb._menutree.stock

    # Handle all input but what we're looking for.
    if not raw_string.isdigit() or not 0 <= int(raw_string) - 1 <= len(stock) -1:
        caller.msg("Select an item by it's listed number.")
        return "choice_node"
    return "confirmation_node"


def confirmation_node(caller, raw_string):
    """
    Choice will be in one of three forms.
    If buying a standard item - details in master item list:
        choice={"key":"Pokeball"}
    If buying a unique item - details on seller:
        choice={"key":"T-Shirt"
                "desc":"The description..."}
    If selling an item:
        choice=object

    If buying stock will be:
        stock = [{key: "Item"}, # Normal item, details in master itemlist
                {key: "Item", desc: "Desc"...}] # Custom item, details in dict.
    If Selling stock will be:
        stock = [[item1, item1], [item2, item2],...]
    """
    top_string = """
                ◓═══════════════════════════════◓══MONEY══◓
                ║ xAxxxxxxxxxxxxxxxxxxxxxxxxxxx ║ xBxxxxx ║
                ║ xxxxxxxxxxxxxxxxxxxxxxxxxxxxx ◓═════════◓
                """
    #           ║ Description of the target item.         ║
    bot_string = """\n
                ◓═════════════════════════════════════════◓
                ║  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
                ║  xAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
                ║ <Back                                   ║
                ◓═════════════════════════════════════════◓
                """

    buying = caller.ndb._menutree.display_message == "BUY"
    stock = caller.ndb._menutree.stock

    # If we've been sent back to this node, use existing values.
    if caller.ndb._menutree.choice_values:
        values = caller.ndb._menutree.choice_values
        key = values["key"]
        desc = values["desc"]
        price = values["price"]
        raw_string = values["raw_string"]

    # Came here anew, derive required values and save to Evmenu.
    else:
        # Get Choice values - Dictionary if BUY or object if SELL
        choice = stock[int(raw_string)-1]
        key = choice["key"] if buying else choice[0].key
        desc = choice.get("desc") if buying else choice[0].db.desc
        desc = ITEMLIST[key]["desc"] if not desc else desc
        try:
            if buying:
                price = choice.get("price")
                price = ITEMLIST[key]["price"] if not price else price
            else:
                price = choice[0].attributes.get("price")
                price = ITEMLIST[key]["price"] if not price else price
                price /= 2
        except:
            price = None

        # Save Choice values to menutree.
        caller.ndb._menutree.choice_values = {
            "raw_string": raw_string,
            "choice": choice,
            "key": key,
            "desc": desc,
            "price": price
        }

    # Handle Table.
    shopmsg = "That will be {}. Ok? \nHow many would you like?" if buying \
        else "I can pay you {} for that.\nHow many would you like to sell?"
    shopmsg = shopmsg.format(price) if price else "I cannot buy that."

    top = EvForm(form={"FORM": top_string},
                 cells={"A": key,
                        "B": str(caller.db.money)})
    middle = EvTable(desc, width=43,
                     border_right_char="║", border_left_char="║",
                     border_top=0, border_bottom=0)
    bot = EvForm(form={"FORM": bot_string},
                 cells={"A": shopmsg})

    form = str(top) + str(middle) + str(bot)

    # Handle Mandatory Options
    options = [{"key": ("back", "b", "exit", "e"),
                "goto": "choice_node"}]

    # Optional Options: Only allow transaction if price obtainable.
    if price:
        options.append({"key": "_default",
                        "goto": "complete_transaction"})

    return form, options


def complete_transaction(caller, raw_string):
    """
    If everything in order, complete transaction and return caller to choice
    node where they left off. Else, return to confirmation node to try again.

    choice_values["choice"] = [item1, item2] # If selling

    If buying stock will be:
        stock = [{key: "Item"}, # Normal item, details in master itemlist
                {key: "Item", desc: "Desc"...}] # Custom item, details in dict.
    If Selling stock will be:
        stock = [[item1, item1], [item2, item2],...]
    """
    string = """\n
                ◓═════════════════════════════════════════◓
                ║  xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
                ║  xAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
            """

    buying = caller.ndb._menutree.display_message == "BUY"
    stock = caller.ndb._menutree.stock
    choice_values = caller.ndb._menutree.choice_values

    # Handle all input but what we're looking for.
    limit = 99 if buying else len(choice_values["choice"])
    if not raw_string.isdigit() or not 1 <= int(raw_string) <= limit:
        form = EvForm(form={"FORM": string},
                      cells={"A": "Select an amount between 1 and " + str(limit) + "..."})
        options = {"key": "_default",
                   "goto": "confirmation_node"}
        return str(form), options

    # Handle not affording it.
    if buying and choice_values["price"] * int(raw_string) > caller.db.money:
        form = EvForm(form={"FORM": string},
                      cells={"A": "You cannot afford that amount..."})
        options = {"key": "_default",
                   "goto": "confirmation_node"}
        return str(form), options

    # Take necessary actions.
    if buying:
        if len(choice_values["choice"]) == 1:
            create_item(choice_values["key"],
                        number=int(raw_string),
                        location=choice_values.get("location", caller))

        else:
            create_item(
                choice_values.pop("key", None),
                number=int(raw_string),
                typeclass=choice_values.pop("typeclass", None),
                location=choice_values.pop("location", caller),
                home=choice_values.pop("home", None),
                permissions=choice_values.pop("permissions", None),
                locks=choice_values.pop("locks", None),
                aliases=choice_values.pop("aliases", None),
                tags=choice_values.pop("tags", None),
                destination=choice_values.pop("destination", None),
                report_to=choice_values.pop("report_to", None),
                nohome=choice_values.pop("nohome", False),
                **choice_values
            )
        # Take Money
        caller.db.money -= choice_values["price"] * int(raw_string)

    if not buying:
        for i in xrange(int(raw_string)):
            choice_values["choice"][i].delete()
            # Give Money
            caller.db.money += choice_values["price"] * int(raw_string)

    form = EvForm(form={"FORM": string},
                  cells={"A": "Here you are!\nThank you!.."})
    options = {"key": "_default",
               "goto": "choice_node"}
    return str(form), options


def quit_node(caller):
    return None, None
