
# -*- coding: utf-8 -*-

"""
Test Town Batchcode

            ΘΘΘΘΘΘΘΘΘΘΘΘΘΘ
            Θ▓▓▓▓▓▓▓▓▓▓▓▓Θ
            Θ▓▒▒┌────┐▒▒▓Θ
            Θ▓▒▒╞════╡▒▒▓Θ
            Θ▓▒▒│    │▒▒▓Θ
            Θ▓▒▒└─■──┘▒▒▓Θ
ΘΘΘΘΘΘΘΘΘΘΘΘΘ▓▒▒▒▒1▒▒▒▒▒▓ΘΘΘΘΘΘΘΘΘΘΘΘΘ
Θ▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▓▓▓▓▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓Θ
Θ▓▒▒┌────┐▒▒▒▒▒▓▓▒▒▒▒▓▓▒▒▒▒▒┌────┐▒▒▓Θ
Θ▓▒▒╞════╡▒▒▒▒▓▒▒▒▓▓▒▒▒▓▒▒▒▒╞════╡▒▒▓Θ
Θ▓▒▒│    │▒▒▒▒▓▓▓▓▒▒▓▓▓▓▒▒▒ │    │▒▒▓Θ
Θ▓▒▒└─■──┘▒▒▒▒▓▒▒▒▓▓▒▒▒▓▒▒▒ └─■──┘▒▒▓Θ
Θ▓▒▒▒▒2▒▒▒▒▒▒▒▒▓▓▒3▒▒▓▓▒▒▒▒▒▒▒4▒▒▒▒▒▓Θ
Θ▓▓▓▓▓▓▓▓▓▓▓▓▓▒▒▒▓▓▓▓▒▒▒▓▓▓▓▓▓▓▓▓▓▓▓▓Θ
ΘΘΘΘΘΘΘΘΘΘΘΘΘ▓☼☼☼☼☼☼☼☼☼☼▓ΘΘΘΘΘΘΘΘΘΘΘΘΘ
            Θ▓☼☼☼☼☼☼☼☼☼☼▓Θ
            Θ▓☼☼☼☼☼☼☼☼☼☼▓Θ
            Θ▓☼☼☼☼☼☼☼☼☼☼▓Θ
            Θ▓☼☼☼☼5☼☼☼☼☼▓Θ
            Θ▓▓▓▓▓▓▓▓▓▓▓▓Θ
            ΘΘΘΘΘΘΘΘΘΘΘΘΘΘ

"""

from evennia import create_object
from typeclasses import rooms, exits, objects_computer, characters_trainers

# INITIAL AREA SETUP
laboratory = create_object(rooms.Room, key="Laboratory")
apartments = create_object(rooms.Room, key="Apartment Complex")
square = create_object(rooms.Room, key="Town Square")
supermarket = create_object(rooms.Room, key="Supermarket")
wilderness = create_object(rooms.Room, key="wilderness")

#---------REMOVE----------------
caller.msg(str(square.id))

"""
1. Laboratory

    ΘΘΘΘΘΘΘΘΘΘΘΘΘΘ
    Θ▓▓▓▓▓▓▓▓▓▓▓▓Θ
    Θ▓▒▒┌────┐▒▒▓Θ
    Θ▓▒▒╞════╡▒▒▓Θ
    Θ▓▒▒│    │▒▒▓Θ
    Θ▓▒▒└─■──┘▒▒▓Θ
    Θ▓▒▒▒▒1▒▒▒▒▒▓Θ
    ▼▼▼▼▼▼▼▼▼▼▼▼▼▼
    
"""
# ROOM DETAILS
laboratory.db.desc = ("A large room containing all manner of scientific "
                      "machinery of complicated function and design. Of these "
                      "devices, a computer stands out in the corner of the "
                      "room. Along the rear wall is a series of bookshelves "
                      "with posters hanging in the spaces in between. The"
                      " room itself appears immaculately maintained - "
                      "everything having a place. With the exception however "
                      "of a table standing in the centre of the room, "
                      "although at the moment there is nothing on it.")

# EXITS
laboratory_ex1 = create_object(exits.Exit, key="South", aliases=["s"],
                               location=laboratory, destination=square)

# NPCS/OBJECTS
prof_oak = create_object(characters_trainers.Trainer, key="Professor Oak",
                         aliases=["Prof", "Oak"], location=laboratory)
prof_oak.locks.add("view:attr(party)")
prof_oak.db.desc = """\
Old man in a white lab coat.
"""
prof_oak.db.conversation = """
def node1(caller):
    text = "%s, raise your young POKEMON by making it fight!" % caller.key
    options = None
    return utils_text.nobordertext(text, "Prof Oak"), options

conversation = {"node1": node1}
"""

lab_aide = create_object(characters_trainers.Trainer, key="Lab Aide",
                         aliases=["Aide"], location=laboratory)
lab_aide.db.desc = """\
Young man in a white lab coat.
"""
lab_aide.db.conversation = """
def node1(caller):
    text = "I study POKEMON as PROF.OAK'S AIDE."
    options = None
    return utils_text.nobordertext(text, "Lab Aide"), options

conversation = {"node1": node1}
"""

oak_pc = create_object(objects_computer.Computer, key="Computer",
                       aliases=["PC"], location=laboratory)

"""
2. Apartments

    ΘΘΘΘΘΘΘΘΘΘΘΘΘ
    Θ▓▓▓▓▓▓▓▓▓▓▓▓►
    Θ▓▒▒┌────┐▒▒▒►
    Θ▓▒▒╞════╡▒▒▒►
    Θ▓▒▒│    │▒▒▒►
    Θ▓▒▒└─■──┘▒▒▒►
    Θ▓▒▒▒▒2▒▒▒▒▒▒►
    Θ▓▓▓▓▓▓▓▓▓▓▓▓►
    ΘΘΘΘΘΘΘΘΘΘΘΘΘ

"""
# ROOM DETAILS
apartments.db.desc = ("The front foyer of an apartment complex.")

# EXITS
apartments_ex1 = create_object(exits.Exit, key="East", aliases=["e"],
                               location=apartments, destination=square)
apartments_ex2 = create_object(exits.ApartmentEntry, key="Up", aliases=["u"],
                               location=apartments, destination=None)

# NPCS/OBJECTS
agent = create_object(characters_trainers.Trainer, key="Real Estate Agent",
                      aliases=["Agent"], location=apartments)
agent.db.desc = """\
A well dressed business man.
"""
agent.db.conversation = """
def node1(caller):
    if caller.location in caller.db.property_owned:
        text = ("I hope you're enjoying your apartment!..")
        return utils_text.nobordertext(text, "Agent"), None

    text = ("Would you like your very own apartment?.. [Y/N]")
    options = {"key": "_default",
               "goto": "node2"}
    return utils_text.nobordertext(text, "Agent"), options

def node2(caller, raw_string):
    if raw_string not in ["yes", "y"]:
        text = ("Perhaps next time!")
        return utils_text.nobordertext(text, "Agent"), None

    from evennia import create_object
    from typeclasses import rooms_apartments, exits
    
    room = create_object(rooms_apartments.Apartment, key=caller.key+"'s Room",
                         location=None)
    room.db.location = caller.location
    room.db.owner = caller
    caller.db.property_owned[caller.location] = room

    text = ("Here you go..")
    options = {"key": "_default",
               "goto": "node3"}
    return utils_text.nobordertext(text, "Agent"), None

conversation = {"node1": node1,
                "node2": node2}
"""

"""
3. Town Square

     ▲▲▲▲▲▲▲▲▲▲▲▲
    ◄▓▒▒▒▓▓▓▓▒▒▒▓►
    ◄▒▒▓▓▒▒▒▒▓▓▒▒►
    ◄▒▓▒▒▒▓▓▒▒▒▓▒►
    ◄▒▓▓▓▓▒▒▓▓▓▓▒►
    ◄▒▓▒▒▒▓▓▒▒▒▓▒►
    ◄▒▒▓▓▒3▒▒▓▓▒▒►
    ◄▓▒▒▒▓▓▓▓▒▒▒▓►
     ▼▼▼▼▼▼▼▼▼▼▼▼

"""
# ROOM DETAILS
square.db.desc = ("A large paved area in the centre of town.")

# EXITS
square_ex1 = create_object(exits.Exit, key="North", aliases=["n"],
                           location=square, destination=laboratory)
square_ex2 = create_object(exits.Exit, key="East", aliases=["e"],
                           location=square, destination=supermarket)
square_ex3 = create_object(exits.TestAreaExit, key="South", aliases=["s"],
                           location=square, destination=wilderness)
square_ex4 = create_object(exits.Exit, key="West", aliases=["w"],
                           location=square, destination=apartments)

# NPCS/OBJECTS

"""
4. Supermarket

     ΘΘΘΘΘΘΘΘΘΘΘΘΘ
    ◄▓▓▓▓▓▓▓▓▓▓▓▓Θ
    ◄▒▒▒┌────┐▒▒▓Θ
    ◄▒▒▒╞════╡▒▒▓Θ
    ◄▒▒ │    │▒▒▓Θ
    ◄▒▒ └─■──┘▒▒▓Θ
    ◄▒▒▒▒▒4▒▒▒▒▒▓Θ
    ◄▓▓▓▓▓▓▓▓▓▓▓▓Θ
     ΘΘΘΘΘΘΘΘΘΘΘΘΘ

"""
# ROOM DETAILS
supermarket.db.desc = ("Inside a large building with rows and rows of items.")

# EXITS
supermarket_ex1 = create_object(exits.Exit, key="West", aliases=["w"],
                           location=supermarket, destination=square)

# NPCS/OBJECTS
nurse_joy = create_object(characters_trainers.Trainer, key="Nurse Joy",
                          aliases=["Nurse", "Joy"], location=supermarket)
nurse_joy.db.desc = """\
A young woman in a nurse outfit.
"""
nurse_joy.db.conversation = """
def node1(caller):
    if not caller.party.list:
        text = ("Welcome to our Pokemon Centre.\\n"
                "We heal your Pokemon back to perfect health!")
        return utils_text.nobordertext(text, "Nurse Joy"), None

    text = ("Welcome to our Pokemon Centre.\\n"
            "We heal your Pokemon back to perfect health!\\n"
            "Shall we heal your Pokemon?.. [Y/N]")
    options = {"key": "_default",
               "goto": "node2"}
    return utils_text.nobordertext(text, "Nurse Joy"), options

def node2(caller, raw_string):
    if raw_string not in ["yes", "y"]:
        text = ("We hope to see you again!")
        return utils_text.nobordertext(text, "Nurse Joy"), None

    text = ("Ok. We'll need your Pokemon..")
    options = {"key": "_default",
               "goto": "node3"}
    return utils_text.nobordertext(text, "Nurse Joy"), options

def node3(caller):
    for pokemon in caller.party:
        pokemon.health.fill()
    text = ("Thank you! Your Pokemon are fighting fit!\\n"
            "We hope to see you again!")
    return utils_text.nobordertext(text, "Nurse Joy"), None

conversation = {"node1": node1,
                "node2": node2,
                "node3": node3}
"""

attendant = create_object(characters_trainers.Trainer, key="Attendant",
                          location=caller.location)
attendant.db.desc = """\
A young male shop attendant.
"""

attendant.db.stock = [
    {"key": "Pokeball"},
    {"key": "White Socks",
     "desc": "A pair of white socks.",
     "price": 50},
    {"key": "Greatball"},
    {"key": "Black Socks",
     "desc": "A pair of black socks.",
     "price": 50},
]

attendant.db.conversation = """
def node1(caller):
    from world import shopmenu
    from evennia.utils.evmenu import EvMenu

    EvMenu(caller, "world.shopmenu", startnode="start_node",
       cmd_on_exit="look", node_formatter=shopmenu.node_formatter,
       options_formatter=shopmenu.options_formatter,
       display_message="May I help you?", page=0,
       npc=caller.ndb._menutree.npc)

    return None, None

conversation = {"node1": node1}
"""

"""
5. Wilderness

     ▲▲▲▲▲▲▲▲▲▲▲▲
    Θ▓☼☼☼☼☼☼☼☼☼☼▓Θ
    Θ▓☼☼☼☼☼☼☼☼☼☼▓Θ
    Θ▓☼☼☼☼☼☼☼☼☼☼▓Θ
    Θ▓☼☼☼☼☼☼☼☼☼☼▓Θ
    Θ▓☼☼☼☼5☼☼☼☼☼▓Θ
    Θ▓▓▓▓▓▓▓▓▓▓▓▓Θ
    ΘΘΘΘΘΘΘΘΘΘΘΘΘΘ

"""
# ROOM DETAILS
wilderness.db.desc = ("An unkept area overgrown with long grass.")

# EXITS
wilderness_ex1 = create_object(exits.Exit, key="North", aliases=["n"],
                           location=wilderness, destination=square)

# NPCS/OBJECTS