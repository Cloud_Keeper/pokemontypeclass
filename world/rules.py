# encoding=utf-8
"""

Holds functions which are used by other modules to implement the games
consistent rules.

We've decided on Gen 3 and the most balanced Gen.

"""

import math
import random

# Pokemon Game Resources

BADGES = ["⎔", "💧", "❂", "❁", "♥", "⌾", "⟒", "⚚"]

# Pokemon generation values.


def calculate_stat(basestat, individualvalue, effortvalue, level, nature=1):
    """Calculates pokemon stats"""
    
    return math.floor(math.floor((2 * basestat + individualvalue
                      + effortvalue) * level / 100 + 5) * nature)


def calculate_health(basestat, individualvalue, effortvalue, level, nature=1):
    """Calculates pokemon stats"""
    
    return math.floor((2 * basestat + individualvalue
                      + effortvalue) * level / 100 + level + 10)

# Pokemon catch rate values


def calculate_catchrate(current_health, max_health, capture_rate, ball_bonus, status):
    """Calculates Pokemon catch rate"""

    return ((3 * max_health - 2 * current_health) + (capture_rate * ball_bonus) / (3 * max_health)) * status


def calculate_wobblerate(catchrate):
    """Calculates Pokemon wobble rate"""

    return 1048560 / math.sqrt(math.sqrt(16711680 / catchrate))

# Pokemon experience values


def calculate_expreq(level, experience_group):
    """Calculates exp value required to level up"""

    if experience_group == "Slow":
        return (5 * level ** 3) / 4

    elif experience_group == "Medium Slow":
        return ((6 / 5) * level ** 3)-( 15 * level ** 2) + 100 * level - 140

    elif experience_group == "Medium Fast":
        return level ** 3

    elif experience_group == "Fast":
        return (4 * level ** 3) / 5

    elif experience_group == "Fluctuating":
        if level <= 15:
            return (level ** 3) * (((level + 1) / 3 + 24) / 50)

        elif 15 < level <= 36:
            return (level ** 3) * ((level + 14) / 50)

        elif 36 < level <= 100:
            return (level ** 3) * ((level / 2 + 32) / 50)

    elif experience_group == "Erratic":
        if level <= 50:
            return (level ** 3 * (100 - level)) / 50

        elif 50 < level <= 68:
            return (level ** 3 * (150 - level)) / 100

        elif 68 < level <= 98:
            return (level ** 3 * ((1911 - 10 * level) / 3)) / 500

        elif 98 < level <= 100:
            return (level ** 3 * (160 - level)) / 100


def calculate_escape(users_speed, opp_speed, attempts):
    rval = random.randint(1, 256)
    val = (((users_speed * 128) / opp_speed) + (30 * attempts)) % 256
    return rval < val
