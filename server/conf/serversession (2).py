"""
ServerSession

The serversession is the Server-side in-memory representation of a
user connecting to the game.  Evennia manages one Session per
connection to the game. So a user logged into the game with multiple
clients (if Evennia is configured to allow that) will have multiple
sessions tied to one Player object. All communication between Evennia
and the real-world user goes through the Session(s) associated with that user.

It should be noted that modifying the Session object is not usually
necessary except for the most custom and exotic designs - and even
then it might be enough to just add custom session-level commands to
the SessionCmdSet instead.

This module is not normally called. To tell Evennia to use the class
in this module instead of the default one, add the following to your
settings file:

    SERVER_SESSION_CLASS = "server.conf.serversession.ServerSession"

"""

from evennia.utils.logger import log_file
from evennia.server.serversession import ServerSession as BaseServerSession


class ServerSession(BaseServerSession):
    """
    This class represents a player's session and is a template for
    individual protocols to communicate with Evennia.

    Each player gets one or more sessions assigned to them whenever they connect
    to the game server. All communication between game and player goes
    through their session(s).
    """

    def data_out(self, **kwargs):
        """
        Sending data from Evennia->Client

        Kwargs:
            text (str or tuple)
            any (str or tuple): Send-commands identified
                by their keys. Or "options", carrying options
                for the protocol(s).

        """

        """
        It's not working because say commands are formatted like this:
        text=(emit_string, {"type": "say"})
        Rather than the usual text= "text"
        So we'll need to check text[0]
        """
        # Custom log code
        filename = self.player.key if self.player else str(self.sessid)
        log_file(str(kwargs.get('text', "")), filename=filename + ".log")

        self.sessionhandler.data_out(self, **kwargs)

    def data_in(self, **kwargs):
        """
        Receiving data from the client, sending it off to
        the respective inputfuncs.

        Kwargs:
            kwargs (any): Incoming data from protocol on
                the form `{"commandname": ((args), {kwargs}),...}`
        Notes:
            This method is here in order to give the user
            a single place to catch and possibly process all incoming data from
            the client. It should usually always end by sending
            this data off to `self.sessionhandler.call_inputfuncs(self, **kwargs)`.
        """
        # Custom log code. {'text': [['test'], {u'cmdid': 0, 'options': {}}]}
        filename = self.player.key if self.player else str(self.sessid)
        log_file(">" + kwargs.get('text', "")[0][0],
                 filename=filename + ".log")

        self.sessionhandler.call_inputfuncs(self, **kwargs)