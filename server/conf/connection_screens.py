# -*- coding: utf-8 -*-
"""
Connection screen

Texts in this module will be shown to the user at login-time.

Evennia will look at global string variables (variables defined
at the "outermost" scope of this module and use it as the
connection screen. If there are more than one, Evennia will
randomize which one it displays.

The commands available to the user when the connection screen is shown
are defined in commands.default_cmdsets.UnloggedinCmdSet and the
screen is read and displayed by the unlogged-in "look" command.

"""

CONNECTION_SCREEN = \
"""
◓══════════════════════════════════════════════════════════════════════════════◓
                                       {b.::.
                                     .;:**'
                                     `
         .:XHHHHk.              db.   .;;.     dH  MX
       oM{yMMMMMMMMM{bM       ~MM..d{yMM{bP :{yMMMMM{bR   M{yM{bM  {yM{bR      ~MRMN
       QM{yMMMM{bb  "{yMM{bX       M{yMMMMM{bP !{yMX' :M{b~   M{yM{bM M{yM{bM  .oo. X{yMM{bM 'MMM
         `{yMMMM{b.  ){yM{b> :X!Hk. M{yMM{bM   X{yMM.o"  {b.  M{yMMMMM{bM X{y?XMM{bM {yMMM{b>!{yMM{bP
          '{yMMM{bb.d{yM{b! X{yM M'?{bM M{yMMMM{bX.`M{yMMMMMMM{b~ M{yM MMM {bX{yM {b`" {yM{bX {yMMXXM{bM
           ~{yMMMM{bM~ X{yMM{b. .{yX{bM X{yM{b`"{yMMM{bb.~*?**~ .M{yMX M t {bM{yMb{boo{yM{bM {yXMMMMM{bP
            ?{yMMM{b>  Y{yMMMMMM{b! M{yM   {b`?{yMM{bRb.    `""`   !{yL{b"{yMMMM{bM {yXM IMM{bM
             M{yMM{bX   "MMMM"  MM       ~%:           !Mh.""' dMI {yIMM{bP
             'M{yMM{b.                                             {yIM{bX
              ~M!M                 {nMUD Version                 {bIMP

                                    {wCONTINUE
                                    NEW GAME{n

            Enter {whelp{n for more info. {wlook{n will re-show this screen.
◓══════════════════════════════════════════════════════════════════════════════◓

"""