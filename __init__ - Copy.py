# Allow for first node having None options ending menu
# Allow for node with None options that's not end node.

class EvMenu(object):

    def __init__():
        """

        """
        # Set up values

        # set up the menu command on the caller
        menu_cmdset = EvMenuCmdSet()
        self.caller.cmdset.add(menu_cmdset, permanent=persistent)

        # start the menu
        self.goto(self._startnode, startnode_input)


    def goto(self, nodename, raw_string):
        """

        """
        nodetext, options = self._execute_node(nodename, raw_string)

        nodetext = "" if nodetext is None else str(nodetext)
        options = [options] if isinstance(options, dict) else options

        self.display_nodetext()
        if not options:
            self.close_menu()
# FIRST MENU TRIGGERS ONLY THIS
# ENDING MENU ALSO TRIGGERS THIS.

    def display_nodetext(self):
        self.caller.msg(self.nodetext, session=self._session)

# THEN IT WAITS FOR A COMMAND.

    class CmdEvMenuNode(Command):
        """

        """
        key = _CMD_NOINPUT

        def func(self):
            """
            Implement all menu commands.
            """

            menu = caller.ndb._menutree

            menu._input_parser(menu, self.raw_string, caller)

    def evtable_parse_input(menuobject, raw_string, caller):
        """

        """
        cmd = raw_string.strip().lower()

        if cmd in menuobject.options:
            # this will take precedence over the default commands
            # below
            goto, callback = menuobject.options[cmd]
            menuobject.callback_goto(callback, goto, raw_string)
        elif menuobject.auto_look and cmd in ("look", "l"):
            menuobject.display_nodetext()
        elif menuobject.auto_help and cmd in ("help", "h"):
            menuobject.display_helptext()
        elif menuobject.auto_quit and cmd in ("quit", "q", "exit"):
            menuobject.close_menu()
        elif menuobject.default:
            goto, callback = menuobject.default
            menuobject.callback_goto(callback, goto, raw_string)
        else:
            caller.msg(_HELP_NO_OPTION_MATCH, session=menuobject._session)

        if not (menuobject.options or menuobject.default):
            # no options - we are at the end of the menu.
            menuobject.close_menu()
# ENDING MENU ALSO TRIGGERS THIS.

    def callback_goto(self, callback, goto, raw_string):
        """

        """
        self.goto(goto, raw_string)