#Welcome to PokeMud#

Pokemon Red/Blue replicated in a MUD.

![Untitled.png](https://bitbucket.org/repo/Mo6EKr/images/1684087439-Untitled.png)

This directory is an [Evennia](http://www.evennia.com/) game directory. Install Evennia as per the [official instructions](https://github.com/evennia/evennia/wiki/Getting-Started) then clone locally, migrate and start with `evennia start`.

## Features ##
1. Pokemon (typeclasses/objects_pokemon.Pokemon)
Pokemon objects contain only the variable information unique to that object (owner, gender etc).
All other information is kept in the pokedex (world/pokedex.pokedex).

Pokemon should be created through the create_pokemon utility method (utils.utils_pokemon.create_pokemon)
This method accepts object values as arguments but completes sanity checks before creation.
The in-game building command @Pokecreate (commands.commands_builders.CmdPokecreate) is an in-game wrapper for calling this method.

Pokemon health is handled through the Healthhandler (typeclasses/handler_health) which maintains all the hooks required for the health system.


Involves:
typeclasses/objects_pokemon.Pokemon - Pokemon typeclass
typeclasses/handler_health.HealthHandler - Changes the objects health attribute respecting gameplay hooks.
world/pokedex.pokedex - Central storage for non-variable pokemon values.
utils.utils_pokemon.create_pokemon - Utility method for creating Pokemon with sanity checks.
commands.commands_builders.CmdCreatePokemon - In-game wrapper for Pokemon creation.

2. Trainer (typeclasses/characters_trainers.Trainer)
Trainer characters used by players. Maintains time played, money and badges. 
Trainers instancs of Pokemon in their party and in their box on the typeclass. These are accessed through the PartyHandler.

Involves:
typeclasses/characters_trainers.Trainer - Trainer typeclass
typeclasses/handler_party.PartyHandler - Changes to trainers party respecting gameplay hooks.

3. Pokeballs (typeclasses/objects_items.Pokeball)
Object that handles adding Pokemon objects to the Trainer objects party.
Object is created using the @createitem command (commands.commands_builder.CmdCreateItem)
Which is an in-game wrapper for the create_item method (utils.utils_items.create_item)
Pokeballs are used through the use command (commands.commands_trainers.CmdUse)
Which calls the use_object method on the Pokeball object itself.
The Pokeball determines whether the catch is successful with reference to world/rules.Calculate_catch & Calculate_wobble
If successful the Pokeball is destroyed, the Pokemon moved to none and added to the trainers party or box via the PartyHandler

4. Menu System (commands.commands_menu)

NOTES:
POKEMON
-Drop Pokemon creates a Pokeball object holding a reference to the Pokemon.
"Get" will trigger "Pokeball contains bla bla". Add to party?
-Give Pokemon creates a "Bla is giving you pokemon. Add to party?"
-trade has to be done at Pokemon centre.

MENU
-start an EvMenu just starting at different nodes.
-have options and _input. if input then exit and run that as exiting command.

IDEA:
-NPC Trainers have Pokemon caught at location. NPC fight you first, then might have a mission, some will reward you 
by giving you a Pokemon.

TODO:
-Put delay:
You throw a Masterball at Bulbasaur
## HERE ##
The Masterball contacts Bulbasaur and opens in a flash of light.
You caught Bulbasaur.
-Need a remove pokemon reference that handles pokemon being in parties etc.