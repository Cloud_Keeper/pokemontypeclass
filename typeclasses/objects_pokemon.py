"""
Pokemon

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from typeclasses.objects import Object
from typeclasses.handler_health import HealthHandler
from typeclasses.handler_moves import MovesHandler
from typeclasses.handler_experience import ExperienceHandler
from world.pokedex import POKEDEX
from world import rules
from evennia.utils import lazy_property


class Pokemon(Object):
    """
    Typeclass for the Pokemon Object.

    """

    @lazy_property
    def health(self):
        """Handler for Health."""
        return HealthHandler(self)

    @lazy_property
    def exp(self):
        """Handler for Experience."""
        return ExperienceHandler(self)

    @lazy_property
    def moves(self):
        """Handler for Experience."""
        return MovesHandler(self)

    def at_object_creation(self):
        """
        Ensure basic Pokemon database values exist. Sane values will be
        calculated by the create_pokemon utility method.
        """
        super(Pokemon, self).at_object_creation()

        # Handled by the HealthHandler
        self.db.health = 1
        self.db.condition = []

        # Handled by the ExpHandler
        self.db.level = 1
        self.db.exp = 0

        # Handled by the MoveHandler
        self.db.moves = []
        self.db.learnlist = []

        # Handled by the EquipHandler
        self.db.limbs = ()
        self.db.slots = {}

        # Current possessor of the Pokemon. Could be borrowed Pokemon.
        self.db.trainer = None
        # Actual owner trainer.
        self.db.owner = [None]

        self.db.iv = [0, 0, 0, 0, 0, 0]
        self.db.ev = [0, 0, 0, 0, 0, 0]
        self.db.gender = None
        self.db.pokeball = None

    def stat(self, int):
        """
        bstat = HP, ATT, DEF, SPATT, SPDEF, SPEED
        Returns:
            stat (int): The Pokemons calculated stat based on int..
        """
        return rules.calculate_stat(self.pokedex("stat")[int], self.db.iv[int],
                                    self.db.ev[int], self.db.level, nature=1)

    def attack(self):
        return self.stat(1)

    def defence(self):
        return self.stat(2)

    def spattack(self):
        return self.stat(3)

    def spdefence(self):
        return self.stat(4)

    def speed(self):
        return self.stat(5)

    def pokedex(self, value):
        """
        Perform a search of the Pokedex for information of this Pokemon.

        Args:
            value (str): Search value. Will be matched against POKEDEX[value]

        Returns:
            match (None, str, list or dict): Will return the matched value
                or None.

        """

        return POKEDEX[self.key].get(value, None)
