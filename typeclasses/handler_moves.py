# -*- coding: UTF-8 -*-
"""
Moves handler module.

The `MovesHandler` provides an interface to manipulate a Pokemon's moves
whilst respecting the various hooks and calls required. The handler is
instantiated as a property on the Pokemon typeclass, with the Pokemon passed
as an argument. It looks for the moves property in the Pokemon's db
attributes handler to initialize itself and provide persistence.

Config Properties:
    list (list): List of current Pokemon objects in party.

Config Requirements:
    obj.db.moves (list): List of lists containing current moves and PP.

Setup:
    To use the MovesHandler, add it to a Pokemon typeclass as follows:

    from typeclass.hander_moves import MovesHandler
      ...
    @property
    def moves(self):
        return MovesHandler(self)

Use:
    Health is added and subtracted using the `heal` and `dmg` methods or
    regular arithmetic operators.

Example usage:
    > self.party.list
    [<Pokemon>, <Pokemon>]
    > self.party.add(<Pokemon>)
    [<Pokemon>, <Pokemon>, <Pokemon>]
    > len(self.party)
    3
    > self.party.alive
    [<Pokemon>]
    > len(self.party.alive)
    1
    >self.party.fainted
    [<Pokemon>, <Pokemon>]

"""
from world.movelist import MOVELIST
from evennia.utils.evmenu import get_input


class MovesException(Exception):
    """
    Base exception class for MovesHandler.

        Args:
            msg (str): informative error message
    """
    def __init__(self, msg):
        self.msg = msg


class MovesHandler(object):
    """Handler for a characters moves.

    Args:
        obj (Character): parent character object. see module docstring
            for character attribute configuration info.

    Properties
    obj.db.moves (list): List of lists containing current moves and PP.

    Methods:

    """

    def __init__(self, obj):
        """
        Save reference to the parent typeclass and check appropriate attributes

        Args:
            obj (typeclass): Pokemon typeclass.
        """
        self.obj = obj

        if not self.obj.attributes.has("moves"):
            msg = '`MovesHandler` requires `db.moves` attribute on `{}`.'
            raise MovesException(msg.format(obj))

    @property
    def list(self):
        """
        Returns current learnt moves in form of list of strings. Order will
        remain the same.

        Returns:
            moves (list): List of moves without PP values.

        Returned if:
            obj.moves.list
        """
        return [move[0] for move in self.obj.db.moves]

    def __str__(self):
        """
        Returns current learnt moves as string.

        Returns:
            moves (str): String of learnt moves names.

        Returned if:
            str(obj.party)
        """
        return ', '.join(self.list)

    def __iter__(self):
        """
        Iterates through move entries e.g. ["tackle", 10].

        Returns:
            move+PP (list): Move information including name [0] and PP [1]

        Returned if:
            for move in obj.moves
        """
        return self.obj.db.moves.__iter__()

    def __len__(self):
        """
        Returns number of current learnt moves. Max 4.

        Returns:
            length (int): Number of moves available to Pokemon.

        Returned if:
            len(obj.moves)
        """
        return len(self.obj.db.moves)

    def get(self, value):
        """
        Return move entry for given int 0-3 or string "move". Returns None if
        no match.

        Returns:
            move (list): Move entry ["tackle", 10] or None.

        Returned if:
            obj.moves.get("tackle")
        """
        if isinstance(value, int):
            return self.obj.db.moves[value]
        else:
            for entry in self.obj.db.moves:
                if entry[0] == value:
                    return entry
                else:
                    return None

    def swap(self, move1, move2):
        """
        Swap Pokemon move positions within move list.

        Returns:

        Returned if:
            obj.party.swap(<move>, <move>)
        """
        reflist = self.list
        mlist = self.obj.db.moves

        if isinstance(move1, int):
            pass
        elif move1 in reflist:
            move1 = reflist.index(move1)
        else:
            return

        if isinstance(move2, int):
            pass
        elif move2 in reflist:
            move2 = reflist.index(move2)
        else:
            return

        mlist[move2], mlist[move1] = mlist[move1], mlist[move2]

    def remove(self, move):
        """
        Remove move from move list. If party would equal zero it fails.

        Returns:
            True (Boolean): Move was removed successfully.
            False (Boolean): Move could not be removed.

        Returned if:
            obj.moves.remove(<move>)
        """

        if isinstance(move, int):
            pass
        elif move in self.list:
            move = self.list.index(move)
        else:
            return

        if len(self.list) > 1:
            del self.obj.db.moves[move]
            return True
        else:
            return False

    def reset(self):
        """
        Reset Pokemons move list based on the last moves it should have learnt
        for it's level.

        Returned if:
            obj.moves.reset()

        Multiple moves in one. Double ups.
        """
        self.obj.db.moves = []
        learn_set = self.obj.pokedex("learn_set")
        moves = []
        for level in xrange(self.obj.exp.level+1):
            if level in learn_set:
                moves.extend(learn_set[level].split(','))

        if len(moves) >= 4:
            moves = moves[-4:]

        for move in moves:
            self.add(move)


    def add(self, value=None):
        """
        Add move to Pokemon's move set.

        This distinguishes between moves learnt via level up (supplied via the
        learnlist attribute on the obj) and moves learnt through other methods
        provided by passing a value.

        If no value passed, it'll get values from the learnlist attribute,
        which is only exists during a level up. The learnlist values
        will be cycled through before checking for evolution.

        Evolution not checked for if value is passed (ie. TMs and HMs)

        Checks if move is in Pokemon move_set.
        Then checks whether Pokemon already knows the move.
        Will not allow doubling up or learning a move not available to Pokemon.

        Args:
            move (str): Name of move Pokemon is to learn. If not provided,
                            checks learnlist attribute.

        Returned if:
            obj.moves.add("move")
        """
        move = value

        if value:
            move = value.strip()

        if not move:
            if self.obj.db.learnlist:
                move = self.obj.db.learnlist.pop(0).strip()

        # No move to learn. If after lvl up, check for evolution.
        if not move and not value:
            if not self.obj.ndb.combat_handler:
                self.obj.exp.detect_evolution(self.obj.exp.level)
            return

        # Move isn't available to Pokemon. (Should never happen)
        if move not in self.obj.pokedex("move_set"):
            if self.obj.db.trainer:
                self.obj.db.trainer.msg(self.obj.key + " is trying to learn "
                                    + move + " but cannot learn that move!")
            # If learning move after lvl up: continue cycle.
            if not value:
                self.add()
            return

        # Move already known to Pokemon. (Should never happen)
        if self.get(move):
            if self.obj.db.trainer:
                self.obj.db.trainer.msg(self.obj.key + " is trying to learn "
                                    + move + " but already knows that move!")
            # If learning moves after lvl up: continue cycle.
            if not value:
                self.add()
            return

        # If space is available: learn move automatically.
        if len(self) < 4:
            if self.obj.db.trainer:
                self.obj.db.trainer.msg(self.obj.key + " learned " + move + "!")
            self.obj.db.moves.append([move, MOVELIST[move]["PP"]])

            # If learning moves after lvl up: continue cycle.
            if not value:
                self.add()
            return

        # If no space: Offer replacing existing move.
        if self.obj.db.trainer:
            self.obj.db.trainer.msg(self.obj.key+" is trying to learn "+move+"!")
            self.obj.db.trainer.msg("But, " + self.obj.key +
                                    " can't learn more than 4 moves!")
            get_input(self.obj.db.trainer,
                      "Delete an older move to make room for " + move + "?",
                      self.add_callback, None, value, move)

    def add_callback(self, caller, prompt, user_input, *args, **kwargs):
        """
        Cycles through, changing the functionality based on the prompt.
        **This should be an EvMenu.

        Args:
            value - Set only if it's learning a move not as a result of lvlup
            move - The move to be learned.

        """
        # "Delete older move to make room for (Move)?
        if prompt.startswith("Delete"):
            if user_input.lower() in ["yes", "y", "accept"]:
                get_input(self.obj.db.trainer,
                          "Which move should be forgotten?\n"+"\n".join(
                                                                    self.list),
                          self.add_callback, None, args[0], args[1])
            else:
                get_input(self.obj.db.trainer,
                          "Abandon learning " + args[1] + "?",
                          self.add_callback, None, args[0], args[1])

        # Which move should be forgotten?
        elif prompt.startswith("Which"):
            if user_input in self.list:
                self.obj.db.moves[self.list.index(user_input)] = \
                                            [args[1], MOVELIST[args[1]]["PP"]]
                self.obj.db.trainer.msg("1, 2 and... Poof!")
                self.obj.db.trainer.msg(self.obj.key + " forgot " +
                                        user_input + "!")
                self.obj.db.trainer.msg("And... ")
                self.obj.db.trainer.msg(self.obj.key + " learnt " +
                                        args[1] + "!")

                # If learning moves after lvl up: continue cycle.
                if not args[0]:
                    self.add()
                return

            else:
                self.obj.db.trainer.msg(user_input + " does not match a known move.")
                get_input(self.obj.db.trainer,
                          "Abandon learning " + args[1] + "?",
                          self.add_callback, None, args[0], args[1])

        # Abandon learning (Move)?
        elif prompt.startswith("Abandon"):
            if user_input.lower() in ["no", "n", "decline"]:
                get_input(self.obj.db.trainer,
                          "Which move should be forgotten?\n" + "\n".join(self.list),
                          self.add_callback, None, args[0], args[1])

            else:
                self.obj.db.trainer.msg(self.obj.key + " did not learn " + args[1])

                # If learning moves after lvl up: continue cycle.
                if not args[0]:
                    self.add()
                return

        return True

    ####################################
    # PP METHODS
    ####################################

    def maxpp(self, move):
        """
        max moves PP.
        """
        if isinstance(move, int):
            pass
        elif move in self.list:
            move = self.list.index(move)
        else:
            return

        return MOVELIST[self.list[move]]["PP"]

    def getpp(self, move):
        """
        return int of moves PP.
        """
        if isinstance(move, int):
            pass
        elif move in self.list:
            move = self.list.index(move)
        else:
            return

        return self.obj.db.moves[move][1]

    def fillpp(self, move):
        """
        add int of moves PP.
        """
        if isinstance(move, int):
            pass
        elif move in self.list:
            move = self.list.index(move)
        else:
            return

        self.obj.db.moves[move][1] = self.maxpp(move)

        return self.getpp(move)

    def setpp(self, move, value=None):
        """
        add int of moves PP.
        """
        if isinstance(move, int):
            pass
        elif move in self.list:
            move = self.list.index(move)
        else:
            return

        if not value:
            return

        if 0 <= value <= self.maxpp(move):
            self.obj.db.moves[move][1] = value
        return self.getpp(move)

    def addpp(self, move, value=1):
        """
        add int of moves PP.
        """
        if isinstance(move, int):
            pass
        elif move in self.list:
            move = self.list.index(move)
        else:
            return

        if self.getpp(move) + value <= self.maxpp(move):
            self.obj.db.moves[move][1] += value
        else:
            self.obj.db.moves[move][1] = self.maxpp(move)

        return self.getpp(move)

    def subpp(self, move, value=1):
        """
        subtract int of moves PP.
        """
        if isinstance(move, int):
            pass
        elif move in self.list:
            move = self.list.index(move)
        else:
            return

        if self.getpp(move) - value >= 0:
            self.obj.db.moves[move][1] -= value
        else:
            self.obj.db.moves[move][1] = 0

        return self.getpp(move)

    # addpp
    # subpp
    # setpp
    # fillpp