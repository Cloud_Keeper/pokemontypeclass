"""
Characters

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
import time
from typeclasses.characters import Character
from typeclasses.handler_party import PartyHandler
from evennia.utils import lazy_property
from evennia.utils.evmenu import EvMenu
from utils import utils_text


class Trainer(Character):
    """
    Typeclass for trainer characters.

    """

    @lazy_property
    def party(self):
        """Handler for Pokemon Party."""
        return PartyHandler(self)

    def at_object_creation(self):
        """
        Called only once, when object is first created
        """
        super(Trainer, self).at_object_creation()

        # Values for the PartyHandler
        self.db.party = []  # [<Pokemon>, <Pokemon>]
        self.db.box = []  # [<Pokemon>, <Pokemon>]

        # Values for Trainer Profile
        self.db.money = 3000
        self.db.time_played = 0.0
        self.db.badges_kanto = [False] * 8

        # Values for Player housing
        self.db.property_owned = {}  # {<location>: <room>}
        self.db.property_accessible = {}  # {<location>: [<room>, <room>]}

    def at_pre_unpuppet(self):
        """
        Called just before the beginning of un-puppeting
        """

        # Update Time Played.
        sessions = self.sessions.get()
        session = sessions[0] if sessions else None
        # Actual time - time connected.
        if session:
            self.db.time_played += time.time() - session.conn_time

    def at_talk(self, caller):
        """
        Called when obj targeted by a talk command.
        """

        def evmenu_null_node_formatter(nodetext, optionstext, caller=None):
            """
            A minimalistic node formatter, no lines, frames or extra lines.
            """
            return nodetext

        def evtable_options_formatter(optionlist, caller=None):
            """
            We don't want any options displayed.
            """
            return ""

        # Response if parent has player or NPC has no conversation set.
        if self.has_player:
            caller.execute_cmd("Say Hi %s" % self.key)
            return

        if not self.db.conversation:
            caller.msg("%s appears too busy to talk right now." % self.key)
            return

        # Telegraph action to room.
        caller.location.msg_contents(caller.key + "speaks to " + self.key,
                                     exclude=caller)

        # Retrieve conversation stored on the object
        conversation = {}
        exec self.db.conversation

        EvMenu(caller, conversation, cmdset_mergetype="Replace",
               startnode="node1", cmdset_priority=1,
               cmd_on_exit="", node_formatter=evmenu_null_node_formatter,
               options_formatter=evtable_options_formatter, npc=self)

    def at_after_move(self, source_location):
        """
        Called after move has completed. Ensures Pokemon follow their trainer.

        Args:
            source_location (Object): Where we came from. This may be `None`.

        """
        super(Trainer, self).at_after_move(source_location)
        if source_location:
            for pokemon in self.party.list:
                if pokemon.location == source_location:
                    pokemon.move_to(self.location)
