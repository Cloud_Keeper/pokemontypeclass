# -*- coding: UTF-8 -*-
"""
Experience handler module.

The `ExperienceHandler` provides an interface to manipulate a Pokemon's exp
and level whilst respecting the various hooks and calls required. The handler
is instantiated as a property on a pokemon typeclass, with the pokemon passed
as an argument. It looks for the level and experience properties in the
pokemon's attributes handler to initialize itself and provide persistence. The
Pokemon's next level threshold is calculated when required rather than stored.

Config Properties:
    current (int): Current health of the Pokemon.

Config Requirements:
    obj.db.level (int): Current level of the Pokemon.
    obj.db.exp (int): Current experience value of the Pokemon.

Setup:
    To use the ExperienceHandler, add it to a pokemon typeclass as follows:

    from typeclass.hander_experience import ExperienceHandler
      ...
    @property
    def exp(self):
        return ExperienceHandler(self)

Use:
    Health is added and subtracted using the `heal` and `dmg` methods or
    regular arithmetic operators.

Example usage:
    # Say self is a Pokemon.
    > @py self.msg(str(self.health.current))
    5
    > @py self.msg(self.health.max)
    10
    > @py self.msg(self.health.percentage)
    50%
    > @py self.msg(self.health.dmg(2))
    5
    > @py self.msg(self.health.heal(4))
    7
    > @py self.msg(self.health-5)
    2
    > @py self.msg(self.health+5)
    7
    > @py self.msg(self.health.fill())
    10
"""
from world.rules import calculate_expreq
from evennia.utils.evmenu import get_input


class ExperienceException(Exception):
    """
    Base exception class for HealthHandler.

        Args:
            msg (str): informative error message
    """
    def __init__(self, msg):
        self.msg = msg


class ExperienceHandler(object):
    """Handler for a characters health.

    Args:
        obj (Character): parent character object. see module docstring
            for character attribute configuration info.

    Properties
        health (integer): returns current health
        condition (list): returns a list of conditions

    Methods:

        add (str): add a condition to the character's condition list.
        remove (str): remove a condition to the character's condition list.
    """

    def __init__(self, obj):
        """
        Save reference to the parent typeclass and check appropriate attributes

        Args:
            obj (typeclass): Pokemon typeclass.
        """
        self.obj = obj

        if not self.obj.attributes.has("level"):
            msg = '`HealthHandler` requires `db.level` attribute on `{}`.'
            raise ExperienceException(msg.format(obj))

        if not self.obj.attributes.has("exp"):
            msg = '`HealthHandler` requires `db.exp` attribute on `{}`.'
            raise ExperienceException(msg.format(obj))

    @property
    def current(self):
        """
        Shows current experience.

        Returns:
            current exp (int): Characters current experience.

        Returned if:
            obj.exp.current
        """
        return int(self.obj.db.exp)

    def __int__(self):
        """
        Shows current experience.

        Returns:
            current exp (int): Characters current experience.

        Returned if:
            int(obj.exp)
        """
        return int(self.obj.db.exp)

    def __str__(self):
        """
        Shows current experience.

        Returns:
            current exp (str): Characters current experience.

        Returned if:
            str(obj.exp)
        """
        return str(self.obj.db.exp)

    def exp_requirement(self, level):
        """
        Calculate experience required for provided level.

        Args:
            level (int): Level to check requirement for.

        Returns:
            exp value (int): Exp for level determined by rules.

        Returned if:
            obj.exp.exp_requirement(100)
        """
        if isinstance(level, int):
            i = int(calculate_expreq(level, self.obj.pokedex("experience_group")))
            i = i if i >= 0 else 0
            return i
        else:
            return NotImplemented

    @property
    def percentage(self):
        """
        Shows current experience formatted as a percentage from last level
        to next level.

        Returns:
            percentage (str): Characters current exp as percentage to next lvl.

        Returned if:
            obj.exp.percent
        """
        return "{}%".format(int((self.current -
                                 self.exp_requirement(self.level)) * 100.0
                                / (self.exp_requirement(self.level+1) -
                                   self.exp_requirement(self.level))))

    # def set(self, value, inbattle=True):
    #     """
    #     Support setting exp triggering level ups.
    #
    #     Args:
    #         value (int): Amount of experience gained.
    #         inbattle (Bool): True if in Battle. Prevents evolution.
    #
    #     Returns:
    #         Nil
    #
    #     Returned if:
    #         obj.exp.add(100)
    #     """
    #     if isinstance(value, int):
    #         self.obj.db.exp += value
    #         self.obj.db.trainer.msg(self.obj.key + " gained " +
    #                              str(value) + " EXP. Points!")
    #         self.detect_levelup(inbattle)
    #     else:
    #         return NotImplemented

    def add(self, value):
        """
        Support addition between exp and int, triggering level ups.

        Args:
            value (int): Amount of experience gained.
            inbattle (Bool): True if in Battle. Prevents evolution.

        Returns:
            Nil

        Returned if:
            obj.exp.add(100)
        """
        if isinstance(value, int):
            self.obj.db.exp += value
            if self.obj.db.trainer:
                self.obj.db.trainer.msg(self.obj.key + " gained " +
                                        str(value) + " EXP. Points!")
            self.detect_levelup()
        else:
            return NotImplemented

    @property
    def level(self):
        """
        Shows current level.

        Returns:
            current level (int): Characters current level.

        Returned if:
            obj.exp.level
        """
        return int(self.obj.db.level)

    def set_level(self, value):
        """
        Support setting exp triggering level ups.

        Args:
            value (int): Amount of experience gained.
            inbattle (Bool): True if in Battle. Prevents evolution.

        Returns:
            Nil

        Returned if:
            obj.exp.add(100)
        """
        if not isinstance(value, int) and 1 <= value <= 100:
            return

        # Set level & EXP & moves.
        self.obj.db.level = value
        self.obj.db.exp = self.exp_requirement(self.level)
        self.obj.moves.reset()

    def detect_levelup(self):
        """
        Compares current exp against requirement for next level. Triggering
        increase in level if conditions are met. Used to detect level up
        without adding to experience.

        Args:
            inbattle (Bool): True if in Battle. Prevents evolution.

        Returns:
            Nil

        Returned if:
            obj.exp.detect_levelup()
        """
        if self.current > self.exp_requirement(self.level+1):
            self.levelup()

    def levelup(self):
        """
        Increases Pokemon level by one, checking for evolution and new moves,
        whether or not experience conditions have been met.

        Args:
            inbattle (Bool): True if in Battle. Prevents evolution.

        Returns:
            Nil

        Returned if:
            obj.exp.levelup()
        """
        # Advance level and adjust exp if required.
        self.obj.db.level += 1
        if self.current < self.exp_requirement(self.level):
            self.obj.db.exp = self.exp_requirement(self.level)

        if self.obj.db.trainer:
            self.obj.db.trainer.msg(self.obj.key + " grew to LV. "
                                    + str(self.level) + "!")

        # Check if moves to learn. Learnlist only filled during exp level ups.
        if self.level in self.obj.pokedex("learn_set"):
            self.obj.db.learnlist = self.obj.pokedex("learn_set")[self.level].split(',')

            if self.obj.db.learnlist:
                self.obj.moves.add()
                return

        # If no moves to learn, and not in battle, check evolve.
        if not self.obj.ndb.combat_handler:
            self.detect_evolution(self.level)

    def detect_evolution(self, condition):
        """
        Determines whether condition supplied triggers an evolution.
        When not in battle this occurs automatically, however in battle this
        call is withheld until the end.

        Args:
            condition (int or str): Value to be checked against Pokedex.
                            i.e. level (5, 16.. etc) or stone ("leaf stone")

        Returns:
            Nil

        Returned if:
            obj.exp.detect_evolution(16)
        """
        evolutions = self.obj.pokedex("evolution")
        if isinstance(condition, int):
            for key in evolutions.keys():
                if isinstance(key, int) and key <= self.level:
                    self.evolve(evolutions[key])

        elif evolutions.has(condition):
            self.evolve(evolutions[condition])
        else:
            return False

    def evolve(self, pokemon):
        """
        Initiates the evolution of the parent Pokemon into the provided
        Pokemon. Choice to interrupt is given to trainer and callback handles
        response.

        Args:
            pokemon (str): Name of Pokemon to evolve into.

        Returns:
            Nil

        Returned if:
            obj.exp.evolve("Ivysaur")
        """
        self.obj.location.msg_contents(self.obj.key +
                                       " begins to glow brightly!")
        self.obj.db.trainer.msg("What? " + self.obj.key + " is evolving!")
        get_input(self.obj.db.trainer, "Interrupt the evolution?",
                  self.evolve_callback, None, pokemon)

    def evolve_callback(self, caller, prompt, user_input, *args, **kwargs):
        """
        Call back for the input obtained from the trainer during evolution.

        Args:
            Pokemon (str): Name of Pokemon self.obj is evolving into.

        Returns:
            Nil

        Returned if:
            obj.exp.evolve("Bulbasaur)
        """
        if user_input.lower() in ["yes", "y", "accept"]:
            self.obj.location.msg_contents(self.obj.key + " stopped evolving!")
            return
        else:
            pokemon = args[0]
            self.obj.location.msg_contents(self.obj.key + " evolved into " +
                                           pokemon + "!")

            self.obj.key = pokemon
            self.obj.db.desc = self.obj.pokedex("description")
            self.obj.db.limbs = self.obj.pokedex("body")
            slots = {}
            for limb in self.obj.db.limbs:
                for slot in limb[1]:
                    slots[slot] = None
            self.obj.db.slots = slots

