"""
Exits

Exits are connectors between Rooms. An exit always has a destination property
set and has a single command defined on itself with the same name as its key,
for allowing Characters to traverse the exit to its destination.

"""
from evennia import DefaultExit
from typeclasses.characters_trainers import Trainer
from world.events_testarea import starting_selection_event as start_event
from evennia.utils import evmenu

class Exit(DefaultExit):
    """
    Exits are connectors between rooms. Exits are normal Objects except
    they defines the `destination` property. It also does work in the
    following methods:

     basetype_setup() - sets default exit locks (to change, use `at_object_creation` instead).
     at_cmdset_get(**kwargs) - this is called when the cmdset is accessed and should
                              rebuild the Exit cmdset along with a command matching the name
                              of the Exit object. Conventionally, a kwarg `force_init`
                              should force a rebuild of the cmdset, this is triggered
                              by the `@alias` command when aliases are changed.
     at_failed_traverse() - gives a default error message ("You cannot
                            go there") if exit traversal fails and an
                            attribute `err_traverse` is not defined.

    Relevant hooks to overload (compared to other types of Objects):
        at_traverse(traveller, target_loc) - called to do the actual traversal and calling of the other hooks.
                                            If overloading this, consider using super() to use the default
                                            movement implementation (and hook-calling).
        at_after_traverse(traveller, source_loc) - called by at_traverse just after traversing.
        at_failed_traverse(traveller) - called by at_traverse if traversal failed for some reason. Will
                                        not be called if the attribute `err_traverse` is
                                        defined, in which case that will simply be echoed.
    """
    pass


class ApartmentEntry(Exit):
    """
    This exit allows players to access their apartments for the given location.
    """

    def at_traverse(self, traversing_object, target_location):
        """
        This implements the actual traversal. The traverse lock has
        already been checked (in the Exit command) at this point.

        Args:
            traversing_object (Object): Object traversing us.
            target_location (Object): Where target is going.
        """
        char = traversing_object
        accessible = []
        if char.location in char.db.property_accessible:
            accessible.extend(char.db.property_accessible[char.location])
        if char.location in char.db.property_owned:
            accessible.insert(0, char.db.property_owned[char.location])

        if not accessible:
            char.msg("You are not allowed up here.")
            return

        if len(accessible) == 1:
            super(ApartmentEntry, self).at_traverse(traversing_object,
                                                    accessible[0])

        else:
            msg = "Which room would you like to enter:\n" + str(accessible)
            evmenu.get_input(char, msg, self.at_traverse_callback,
                             session=char.sessions.all(), accessible=accessible)

    def at_traverse_callback(self, caller, prompt, result, **kwargs):
        """
        TO DO: Allow numbers.
        """
        accessible = [x.key for x in kwargs["accessible"]]
        if result in accessible:
            room = kwargs["accessible"][accessible.index(result)]
            super(ApartmentEntry, self).at_traverse(caller, room)

class ApartmentExit(Exit):
    """
    This exit allows players to access their apartments for the given location.
    """

    def at_traverse(self, traversing_object, target_location):
        """
        This implements the actual traversal. The traverse lock has
        already been checked (in the Exit command) at this point.

        Args:
            traversing_object (Object): Object traversing us.
            target_location (Object): Where target is going.
        """
        super(ApartmentExit, self).at_traverse(traversing_object,
                                               self.location.db.location)


class TestAreaExit(Exit):
    """
    This exit catches new players (Players with no Pokemon) and triggers the
    Pokemon Select scripted event.
    """

    def at_traverse(self, traversing_object, target_location):
        """
        This implements the actual traversal. The traverse lock has
        already been checked (in the Exit command) at this point.

        Args:
            traversing_object (Object): Object traversing us.
            target_location (Object): Where target is going.
        """

        if isinstance(traversing_object,
                      Trainer) and traversing_object.has_player:
            if not traversing_object.party:
                start_event(self, traversing_object)
                return
        super(TestAreaExit, self).at_traverse(traversing_object,
                                              target_location)
