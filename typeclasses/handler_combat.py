# -*- coding: UTF-8 -*-
"""
Combat handler module.

BUGS:
-If battle ends whilst Pokemon is in list before entering battle, battle
reference will remain.

"""
import random
from evennia import DefaultScript
from evennia.commands import cmdset
from commands import command_combat
from evennia.utils import evtable, evmenu
from typeclasses.objects_pokemon import Pokemon
from world import rules
import itertools


class CombatHandler(DefaultScript):
    """
    This implements the combat handler.
    """

    def at_script_creation(self):
        """
        Called when script is first created. Sets up values then idles.

        Args:
            phase [string]: Used by at_start() to determine what phase to
                            initialise trainers for - Challenge / Action.
            rules [dict]: Information given by original battle command.
                          Contains teams/pokemon #/field #.
            trainers [dict]: Stores trainers and their action choices.
            pokemon [dict]: Stores pokemon and their action choices.
            teams [list]: Trainers split into teams for reference.
            pokestats [dict]: Stores pokemon stat or battle that can be altered
        """
        # Script attributes.
        self.key = "combat_handler_%i" % random.randint(1, 1000)
        self.desc = "handles combat"
        self.interval = 60 * 2  # two minute timeout
        self.start_delay = True
        self.persistent = True

        # Battle attributes.
        self.db.phase = "Challenge"
        self.db.rules = {}
        self.db.teams = []
        self.db.trainers = {}
        self.db.pokemon = {}
        self.db.pokestats = {}

    #########################
    # At Battle Initialisation
    #########################

    def init_battle(self, rules, teams, pokemon, phase):
        """
        Initialises battles values and starting state.

        Args:
            phase [string]: Used by at_start() to determine what phase to
                            initialise trainers for - Challenge / Action.
            rules [dict]: Information given by original battle command.
                          Contains party_size, field_size.
            teams [list]: Characters to be involved in the battle split into
                          teams. Set out in the following layout:
                self.db.teams=[[Player1,
                                Player2,
                               [Player3,
                                Player4]]
            trainers: Stores trainers and their action choices. Layout:
                self.db.trainers={Player1:None,
                                  Player2:None,
                                  Player3:None,
                                  Player4:None}
        """
        self.db.phase = phase
        self.db.rules = rules
        self.db.teams = teams
        self.db.pokemon = pokemon
        # Dict to store trainer choices derived from the teams list.
        self.db.trainers = dict.fromkeys(list(itertools.chain.from_iterable(teams)))

        self.at_start()

    def at_start(self):
        """
        This is called on first start but also when the script is restarted
        after a server reboot. We need to re-assign this combat handler to
        all characters as well as re-assign the cmdset.
        """
        if self.db.phase == "Challenge" and self.db.trainers:
            for trainer in self.db.trainers:
                self._init_trainer_challenge(trainer)

        elif self.db.phase == "Action" and self.db.trainers:
            for trainer in self.db.trainers.keys():
                self._init_trainer_action(trainer)

        # Option to skip Challenge phase.
        elif self.db.phase == "Encounter" and self.db.trainers:
            self.db.phase = "Action"
            self.msg_all("Choose your Pokemon!")
            for trainer in self.db.trainers.keys():
                self.db.trainers[trainer] = [None] * self.db.rules["field_size"]
                self._init_trainer_action(trainer)

    #########################
    # Challenge stage
    #########################

    def _init_trainer_challenge(self, trainer):
        """
        Present players with an invitation to accept the battle.

        Players are given a command set with acceptance commands.
        """
        # Set up Trainer.
        trainer.ndb.combat_handler = self

        # Present trainer with challenge.
        trainer.msg("You have been invited to battle.")
        trainer.cmdset.add(command_combat.ChallengeCmdSet)

    def challenge_callback(self, caller, response):
        """
        Receives response from players. Called by acceptance commands.
        Interacts with the trainers dictionary. On Player 1 accepting:
                self.db.trainers={Player1:True,
                                  Player2:None,
                                  Player3:None,
                                  Player4:None}
        """
        if response:
            self.db.trainers[caller] = True

            # If all have accepted: Trigger Action Phase
            if all(self.db.trainers.values()):
                self.ndb.challenge_turn = True
                self.db.phase = "Action"
                self.force_repeat()

        # If anyone declines, rescind invitations and cancel battle.
        else:
            self.msg_all(caller.key + " has declined to battle.",
                         exceptions=[caller])
            self.stop()

    #########################
    # Action stage
    #########################

    def _init_trainer_action(self, trainer):
        """
        Creates pokemon commands during combat.
        """
        trainer.ndb.combat_handler = self
        trainer.cmdset.add(command_combat.ActionCmdSet)

    def action_callback(self, key, action=None, trainer=None,
                   pokemon=None, target=None, rp=None):
        """
        Receives responses from players using the action commands.

        At the beginning of this phase the trainer dict should have a list
        with None for each pokemon the trainer can field. The pokemon dict
        takes the role of the trainer dict in the Challenge stage, holding
        the actions of Pokemon until the end of turn and should be empty:
                trainer = {Player1:[None],
                           Player2:[None]}
                pokemon = {}

        This callback goes through the following steps:
        1. Receive an action from a trainer.
        2. If all trainers have pokemon:
               If Pokemon entering the battle, do it now.
           else: return to step 1.
        4. If all pokemon have actions (or suffering cool down)
               Resolve round and return to step 1 for fresh actions.
           else: return to step 1.

        Because all other battle commands requires an active Pokemon on the
        field, at first the only command available is to 'select'.

        Trainers select Pokemon through the 'select' command, which are then
        stored on the trainer as a list with the roleplay elements to be
        stored until the pokemon enters the battle just before turn resolution.
        This is so all Pokemon are sent out together:
            trainer = {Player1:[[Pokemon1, target, rp]],
                       Player2:[[Pokemon2, target, rp]]}
            Pokemon = {Pokemon1: None,
                       Pokemon2: None}

        Selecting Pokemon are handled just before turn resolution. This is
        because Pokemon selected at the beginning of battle or to replace a
        fainted Pokemon can act immediately.
        The select command mid way through would look like this:
            trainer = {Player1:[Pokemon1],
                       Player2:[Pokemon2]}
            Pokemon = {Pokemon1: ["Attack", "Tackle", <trainer>, <pokemon>, target, RP],
                       Pokemon2: ["Swap", None, <trainer>, <pokemon>, target, RP]}

        A turn starting after a Pokemon had fainted would mirror the start of
        battle for that trainer, with the only valid action being 'select'
        The battle values would look like this:
            trainer = {Player1:[Pokemon1],
                       Player2:[None]}
            Pokemon = {Pokemon1: None}

        The turn will only end properly when all trainers have pokemon and
        all pokemon have actions.

        Commands:
            Select: The only available command when trainer has no Pokemon.
                    Command used to add a Pokemon to a 'None' field position.
                    Unique as there exists no Pokemon to hold this command in
                    the Pokemon dictionary and so is stored in trianer instead.
            Swap: Command used to replace a non-fainted Pokemon in battle.
            Fight:
            Item:
            Run:

        [ATTACK, "Tackle", trainer, attacker, target, RP]
        [RUN, none, trainer, none, none, none]
        [Select, none, trainer, pokemon, target, RP]

        * I think I want fainted Pokemon to stay out until swapped out manually
        for role play purposes.
        * Include 'return' in select loop? So you can do return and select
        separately?
        """
        # 1. RECEIVE ACTION FROM TRAINER.

        # Selected Pokemon are stored in the trainer dictionary until turn end.
        if key == "Select":
            # Find first available slot or target pokemon in trainer dict.
            field = self.db.trainers[trainer]
            for i in xrange(len(field)):
                if field[i] == target:
                    # Fill space with list showing pokemon is entering battle.
                    self.db.trainers[trainer][i] = [pokemon, target, rp]
                    break

        # Otherwise attach the action to the Pokemon.
        else:
            self.db.pokemon[pokemon] = [key, action, trainer, pokemon, target, rp]

        # 2. CHECK IF ALL FIELD POSITIONS HAVE POKEMON.

        if all(list(itertools.chain.from_iterable(self.db.trainers.values()))):
            # Check the field positions for each trainer.
            for trainer, field in self.db.trainers.iteritems():
                for i in xrange(len(field)):
                    # If not a Pokemon, it's a Pokemon ready to enter battle.
                    if not isinstance(field[i], Pokemon):
                        # Unpack Pokemon [Pokemon1, target, rp]
                        pokemon = field[i][0]
                        target = field[i][1]
                        roleplay = field[i][2]

                        # Swap in Pokemon reference.
                        self.db.trainers[trainer][i] = pokemon

                        # Handle roleplay entries.
                        if roleplay:
                            for text in roleplay:
                                if text[0] in ["'"]:
                                    trainer.execute_cmd("Say " + text[1:])
                                else:
                                    trainer.execute_cmd("Pose " + text)
                        else:
                            trainer.execute_cmd("Say I choose you! " + pokemon.key + "!")

                        # Enter Pokemon into battle
                        if not pokemon.location:
                            trainer.party.cast(pokemon)
                        self.msg_all(pokemon.key + " enters the battle!")
                        trainer.msg("Select an action!")

                        # Exchange references between battle and Pokemon.
                        self.db.pokemon[pokemon] = None
                        pokemon.ndb.combat_handler = self

                        if pokemon not in self.db.pokestats:
                            # [ATT, DEF, SPATT, SPDEF, SPD, EV, ACC, FLEE #]
                            self.db.pokestats[pokemon] = [
                                pokemon.attack(),
                                pokemon.defence(),
                                pokemon.spattack(),
                                pokemon.spdefence(),
                                pokemon.speed(),
                                100,
                                100,
                                1
                            ]

                        # Add Pokemon commands to trainer.
                        self._init_pokemon_command(trainer, pokemon)
        else:
            return

        # 3. CHECK IF ALL POKEMON HAVE ACTIONS.

        if all(self.db.pokemon.values()):
            self.ndb.action_turn = True
            self.db.phase = "Action"
            self.force_repeat()

    #########################
    # Resolve Turn
    #########################

    def action_resolution(self):
        """
        trainer = {Player1:[Pokemon1],
                   Player2:[Pokemon2]}
        Pokemon = {Pokemon1: ["Attack", "Tackle", <trainer>, <pokemon>, target, RP],
                   Pokemon2: ["Swap", None, <trainer>, <pokemon>, <target>, RP]

        Swap: Command used to replace a non-fainted Pokemon in battle.
        Fight:
        Item:
        Run:
        """
        # Sort Pokemon actions by speed.
        actions = dict(self.db.pokemon)
        order = sorted(actions.keys(), key=lambda x: x.speed(), reverse=True)

        # Run implementation.
        # Restriction on running from trainers done in run command.
        for pokemon in order:
            if actions[pokemon][0] == "Run":
                # Find fastest opponent for calculations.
                fastest_opponent = None

                # First find allies to rule out.
                owner = actions[pokemon][2]
                allies = []
                for team in self.db.teams:
                    if owner in team:
                        allies = team
                        break

                # Now find first entry in order who is not an ally.
                for opponent in order:
                    if opponent is not pokemon and \
                                    opponent.db.trainer not in allies:
                        fastest_opponent = opponent
                        break

                # If successful end battle, else continue.
                if rules.calculate_escape(pokemon.speed(),
                                          fastest_opponent.speed(),
                                          self.db.pokestats[pokemon][7]):
                    self.msg_all(owner.key + " has fled the battle!",
                                 exceptions=[owner])
                    owner.msg("You have fled the battle!")
                    self.msg_all("The battle has ended.")
                    self.stop()
                else:
                    self.db.pokestats[pokemon][7] += 1
                    self.msg_all(owner.key +
                                 " tries to flee but cannot get away!",
                                 exceptions=[owner])
                    owner.msg("You try to flee but cannot get away!")

        # Swap implementation.
        for pokemon in order:
            if actions[pokemon][0] == "Swap":
                value = actions[pokemon]
                # Remove target
                self._cleanup_pokemon(value[2], value[3])

                # Handle roleplay entries.
                if value[5]:
                    for text in value[5]:
                        if text[0] in ["'"]:
                            value[2].execute_cmd("Say " + text[1:])
                        else:
                            value[2].execute_cmd("Pose " + text)
                else:
                    value[2].execute_cmd("Say That's enough " + value[4].key + "!")
                    value[2].execute_cmd("Say Go " + value[3].key + "!")

                self.msg_all(value[3].key + " leaves the battle!")

                # Handle adding Pokemon to battle
                if not value[4].location:
                    value[2].party.cast(value[4])

                field = self.db.trainers[value[2]]
                for i in xrange(len(field)):
                    if not field[i]:
                        field[i] = value[4]
                        break
                self._init_pokemon_command(value[2], value[4])
                self.db.pokemon[value[4]] = None
                self.msg_all(value[4].key + " enters the battle!")

        # Item implementation.
        for pokemon in order:
            if actions[pokemon][0] == "Item":
                pass

        # Fight implementation.
        for pokemon in order:
            if actions[pokemon][0] == "Fight":
                # Pokemon = {Pokemon1: ["Attack", "Tackle", <trainer>, <pokemon>, target, RP],
                pass

    #########################
    # Repeat Script
    #########################

    def at_repeat(self):
        """
        This is called every self.interval seconds (turn timeout) or
        when force_repeat is called (because everyone has entered their
        commands). We know this by checking the existence of the
        `normal_turn_end` NAttribute, set just before calling
        force_repeat.

        This prepares the teams for the next round.

        """
        if self.ndb.challenge_turn:
            del self.ndb.challenge_turn
            self.msg_all("Choose your Pokemon!")
            for trainer in self.db.trainers:
                trainer.cmdset.remove(command_combat.ChallengeCmdSet)
                self.db.trainers[trainer] = [None] * self.db.rules["field_size"]
                self._init_trainer_action(trainer)

        elif self.ndb.action_turn:
            del self.ndb.action_turn
            self.action_resolution()
            self.msg_all("\nSelect an action!")
            self.db.pokemon = dict.fromkeys(self.db.pokemon.keys())

        else:
            # turn timeout
            self.msg_all("Combat has ended due to inaction.")
            self.stop()

    #########################
    # Script Utilities
    #########################

    # Return list of pokemon for given trainer.
    # Return if against trainer
    # Return if all positions filled with Pokemon
    # Return whether field position available to trainer.

    def msg_all(self, message, exceptions=()):
        """
        Send message to all combatants
        """
        for trainer in self.db.trainers:
            if trainer not in exceptions:
                trainer.msg(message)

    def _init_pokemon_command(self, trainer, pokemon):
        actioncmdset = cmdset.CmdSet(None)
        actioncmdset.key = str(pokemon.id) + 'CmdSet'
        actioncmdset.priority = 101
        actioncmdset.mergetype = "Merge"
        actioncmdset.duplicates = True
        cmd = command_combat.PokemonCommand(
            key=pokemon.key.lower(),
            aliases=pokemon.aliases.all(),
            obj=pokemon)
        actioncmdset.add(cmd)
        trainer.cmdset.add(actioncmdset)

    def debug(self, caller):
        """

        """
        caller.msg("Phase")
        caller.msg(str(self.db.phase))
        caller.msg("Rules")
        caller.msg(str(self.db.rules))
        caller.msg("Teams")
        caller.msg(str(self.db.teams))
        caller.msg("Trainers")
        caller.msg(str(self.db.trainers))
        caller.msg("Pokemon")
        caller.msg(str(self.db.pokemon))
        caller.msg("Pokemon stats")
        caller.msg(str(self.db.pokestats))

    #########################
    # Finish Script
    #########################

    def at_stop(self):
        """
        Called just before the script is stopped/destroyed.
        Conducts cleanup on each trainer connected to handler.
        """
        for trainer in self.db.trainers:
            self._cleanup_trainer(trainer)

    def _cleanup_trainer(self, trainer):
        """
        Remove the handler reference and cmdsets from character.
        Remove handler reference from Pokemon.
        """
        del trainer.ndb.combat_handler
        trainer.cmdset.remove(command_combat.ChallengeCmdSet)
        trainer.cmdset.remove(command_combat.ActionCmdSet)
        for pokemon in self.db.trainers[trainer]:
            if isinstance(pokemon, Pokemon):
                del pokemon.ndb.combat_handler
                trainer.cmdset.delete(str(pokemon.id) + 'CmdSet')

    def _cleanup_pokemon(self, trainer, pokemon):
        """
        Use by fainting and swapping a Pokemon.
        """
        del self.db.pokemon[pokemon]
        del pokemon.ndb.combat_handler
        trainer.cmdset.delete(str(pokemon.id) + 'CmdSet')
        field = self.db.trainers[trainer]
        field[field.index(pokemon)] = None

