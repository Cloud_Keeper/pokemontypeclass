
# -*- coding: utf-8 -*-

"""
Characters

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from typeclasses.objects import Object
from evennia.utils.evmenu import EvMenu, null_node_formatter
from evennia.utils.evtable import EvTable
from evennia.utils.evform import EvForm

def evtable_options_formatter(optionlist, caller=None):
    """
    We don't want any options displayed.
    """
    return ""

class Computer(Object):
    """

    """
    def use_object(self, caller, target, quiet=False):
        """
        Method usually called by the use command. Handles the objects effect.
        Must always receive a caller and target although ignores args it
        doesn't need.
        """
        EvMenu(caller, "typeclasses.objects_computer", startnode="start_node",
               cmd_on_exit="look", node_formatter=null_node_formatter,
               options_formatter=evtable_options_formatter,
               display_message=str(caller.key) + " turned on the PC.", page=0)


def start_node(caller):
    string = """
                ◓═════════════════════════════════════════◓
                ║ 1. BILL's PC                            ║
                ║ 2. xAxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 3. PROF. OAK's PC                       ║
                ║ 4. LOG OFF                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║  ccccccccccccccccccccccccccccccccccccc  ║
                ║  c1ccccccccccccccccccccccccccccccccccc  ║
                ║  ccccccccccccccccccccccccccccccccccccc  ║
                ◓═════════════════════════════════════════◓
    """
    form = EvForm(form={"FORM": string},
                  cells={"A": str(caller.key) + "'s PC"},
                  tables={1: EvTable(caller.ndb._menutree.display_message,
                                     border=None, align="c", valign="c")})

    options = (
    {"key": ("1", "bill", "bills", "bills pc", "bill's", "bill's pc"),
     "goto": "bill_node",
     "exec": lambda caller: setattr(caller.ndb._menutree,
                                    "display_message",
                                    "Accessed POKEeMON Storage System.")},

    {"key": (
    "2", str(caller.key), str(caller.key) + "s pc", str(caller.key) + "'s pc"),
     "goto": "bill_node",
     "exec": lambda caller: setattr(caller.ndb._menutree,
                                    "display_message",
                                    "Accessed Item Storage System.")},

    {"key": ("3", "oak", "oaks pc", "oak's pc"),
     "goto": "bill_node",
     "exec": lambda caller: setattr(caller.ndb._menutree,
                                    "display_message",
                                    "Accessed POKeDEX Rating System.")},

    {"key": ("4", "log off", "cancel", "exit", "quit"),
     "goto": "logoff_node"})
    return unicode(form), options


def bill_node(caller):
    string = """
                ◓═════════════════════════════════════════◓
                ║ 1. WITHDRAW PKMN                        ║
                ║ 2. DEPOSIT  PKMN                        ║
                ║ 3. RELEASE  PKMN                        ║
                ║ 4. SEE YA!                              ║
                ║                                         ║
                ◓═════════════════════════════════════════◓
                ║  ccccccccccccccccccccccccccccccccccccc  ║
                ║  c1ccccccccccccccccccccccccccccccccccc  ║
                ║  ccccccccccccccccccccccccccccccccccccc  ║
                ◓═════════════════════════════════════════◓
    """
    form = EvForm(form={"FORM": string},
                  tables={1: EvTable(caller.ndb._menutree.display_message,
                                     border=None, align="c", valign="c")})

    # return unicode(form)

    options = ({"key": ("1", "withdraw"),
                "goto": "pokeselect_node",
                "exec": lambda caller: (setattr(caller.ndb._menutree,
                                                "display_message",
                                                "WITHDRAW"),
                                        setattr(caller.ndb._menutree,
                                                "page",
                                                0))},

               {"key": ("2", "deposit"),
                "goto": "pokeselect_node",
                "exec": lambda caller: setattr(caller.ndb._menutree,
                                               "display_message",
                                               "DEPOSIT")},

               {"key": ("3", "release"),
                "goto": "pokeselect_node",
                "exec": lambda caller: setattr(caller.ndb._menutree,
                                               "display_message",
                                               "RELEASE")},

               {"key": ("4", "see ya", "log off", "cancel", "exit", "quit"),
                "goto": "start_node"})

    return unicode(form), options


def pokeselect_node(caller):
    string = """
                ◓═════ ◓══════════════════════════════════◓
                ║ 1. W ║ x1xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 2. D ║ x2xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 3. R ║ x3xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ║
                ║ 4. S ║ x4xxxxxxxxxxxxxxxxxxx ◓══════════◓
                ║      ║ x5xxxxxxxxxxxxxxxxxxx ║ xAxxxxxx ║
                ◓═════ ║ x6xxxxxxxxxxxxxxxxxxx ║ STATS    ║
                ║    A ║ x7xxxxxxxxxxxxxxxxxxx ║ NEXT PG  ║
                ║      ║ x8xxxxxxxxxxxxxxxxxxx ║ PREV PG  ║
                ║      ║ x9xxxxxxxxxxxxxxxxxxx ║ CANCEL   ║
                ◓═════ ◓═══════════════════════◓══════════◓
    """

    # Set appropriate list.
    list = caller.db.party if \
        caller.ndb._menutree.display_message == "DEPOSIT" else caller.db.box

    # Fill available selections from list.
    page = caller.ndb._menutree.page
    pos = 1
    cells = {"A": caller.ndb._menutree.display_message}
    for i in xrange(page, page + 9):
        if i > len(list) - 1:
            break
        cells[pos] = str(i + 1) + "." + "     "[len(str(i + 1)) + 1:] + list[
            i].key
        pos += 1

    form = EvForm(form={"FORM": string},
                  cells=cells)

    options = ({"key": "_default",
                "goto": "bill_node",
                "exec": "pokeexec_node"},

               {"key": ("next page", "next", "n"),
                "goto": "pokeselect_node",
                "exec": lambda caller: setattr(caller.ndb._menutree,
                                               "page",
                                               caller.ndb._menutree.page + 9)},

               {"key": ("previous page", "previous", "prev", "p"),
                "goto": "pokeselect_node",
                "exec": lambda caller: setattr(caller.ndb._menutree,
                                               "page",
                                               (
                                               caller.ndb._menutree.page - 9) if (
                                                                                 caller.ndb._menutree.page - 9) >= 0 else 0)},

               {"key": ("cancel", "c", "exit", "quit", "log off"),
                "goto": "bill_node"})
    return unicode(form), options


def pokeexec_node(caller, raw_string):
    # Target remains None unless raw_string is "Number" or "<Noun> Number".
    target = None
    if raw_string.isdigit():
        target = int(raw_string)
    elif len(raw_string.split()) > 1 and raw_string.split()[1].isdigit():
        target = int(raw_string.split()[1])
    if not target:
        setattr(caller.ndb._menutree, "display_message",
                "Failure: Select Pokemon by number.")
        return None, None

    # Set list and ensure target exists within it.
    command = caller.ndb._menutree.display_message
    list = caller.db.party if command == "DEPOSIT" else caller.db.box
    if target > len(list):
        setattr(caller.ndb._menutree, "display_message",
                "Failure: Selection does not exist.")
        return None, None

    # Stats menu diversion.
    if raw_string.split()[0] in ("stats", "stat", "s"):
        caller.ndb._menutree.goto("stats_node", "Test")
        return None, None

    if command == "WITHDRAW":
        if len(caller.db.party) >= 6:
            setattr(caller.ndb._menutree, "display_message",
                    "Failure: Party is full.")
            return None, None

        caller.db.party.append(list[target - 1])
        pokemon = list.pop(target - 1)
        setattr(caller.ndb._menutree, "display_message",
                pokemon.key + " is taken out.")
        return None, None

    if command == "DEPOSIT":
        if len(caller.db.party) <= 1:
            setattr(caller.ndb._menutree, "display_message",
                    "Failure: Must have at least 1 Pokemon.")
            return None, None

        caller.db.box.append(list[target - 1])
        pokemon = list.pop(target - 1)
        setattr(caller.ndb._menutree, "display_message",
                pokemon.key + " was stored in Box.")
        return None, None

    if command == "RELEASE":
        pokemon = list.pop(target - 1)
        setattr(caller.ndb._menutree, "display_message",
                pokemon.key + " was released outside.")
        return None, None

    return None, None


def stats_node(caller, raw_string):
    form = raw_string
    options = {"key": "_default",
               "goto": "pokeselect_node"}
    return form, options


def logoff_node(caller):
    return None, None
