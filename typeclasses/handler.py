# -*- coding: UTF-8 -*-
"""
Example Handler module.

A common practice in Evennia development is to use `handlers`.

A handler provides an interface to manage an attribute or collection of
attributes on an object. Say you have a health attribute that you wish
triggered various hooks when added or subtracted to. You would do so through
add() and subtract() methods stored on the typeclass, however as your game
becomes more complex and typeclasses gain more attributes, typeclasses may
become bloated. Handlers allow sane compartmentalization of methods and allow
repetition of method names.

This file contains all the methods and information required to make a handler.
This example code references methods and files in order to demonstrate use.
The handler will not, and is not intended to, work as is.

The handler is instantiated as a property on a typeclass, with the object
passed as an argument. It looks for certain atrributes on the objects db
attributes handler to initialize itself and provide persistence.

Setup:
    To use a Handler, add it to a typeclass as follows:

    from typeclasses.examplehandler import ExampleHandler
      ...
    @property
    def example(self):
        return ExampleHandler(self)

    Add any required attributes to the typeclass at_object_creation hook:

    def at_object_creation(self):
    ...
    self.db.heatlh = 10
    self.db.team = []

Use:
    The handler can now be utilised by calling the defined property's name:
        > @py self.msg(self.example.current)
        10
        > @py self.msg(self.example.max)
        20
"""


class ExampleException(Exception):
    """
    Base exception class for the Handler.

    The name of this class should be changed to match your handler. This
    allows easy identification of exceptions happening from within the handler.
    In this case an ExampleException will come from your ExampleHandler.

        Args:
            msg (str): informative error message
    """
    def __init__(self, msg):
        self.msg = msg


class ExampleHandler(object):
    """
    This is the main Handler class, the class you import and define on the
    typeclass. The example maintains two properties to show case full
    functionality which in practice should probably be treated in
    separate handlers.

    Args:
        obj (Character): parent character object. see module docstring
            for character attribute configuration info.

    Properties
        health (integer): Example value a handler might be used for.
        team (list): Example list a handler might be used for.

    Methods:


    """

    def __init__(self, obj):
        """
        This is a required method. It handles creating a back reference to
        the attached object for easy reference within the handler. Also
        it's a good idea to set up errors if the object doesn't meet the
        necessary requirements.

        Args:
            obj (typeclass): Attached typeclass.
        """
        self.obj = obj

        if not self.obj.attributes.has("health"):
            msg = '`ExamplehHandler` requires `db.health` attribute on `{}`.'
            raise ExampleException(msg.format(obj))

        if not self.obj.attributes.has("condition"):
            msg = '`ExampleHandler` requires `db.health` attribute on `{}`.'
            raise ExampleException(msg.format(obj))

    """
    Properties.
    Properties are methods that are called without arguments. These properties
    normally aren't expected to do anything but do or return what they say.
    These methods and their names are not hardcoded and can provide any
    functionality.

    Properties are called directly (obj.example.current) rather than called as
    functions (obj.example.current()).
    """

    @property
    def current(self):
        """
        Shows current health.

        This method will ensure that whatever type the attribute is, it will
        return in a predictable way.

        Returns:
            current health (int): Objects current health.

        Returned if:
            >obj.example.current
            10
        """
        return int(self.obj.db.health)

    @property
    def max(self):
        """
        Calculate obj's max health.

        This method will cleanly handle a complicated function call and return
        the resulting value.

        Returns:
            max health (int): Max health determined by rules.

        Returned if:
            >obj.example.max
            20
        """
        # return int(game_rules.calculate_health(arg1, arg2, arg3))
        return 20

    @property
    def percentage(self):
        """
        Shows current health formatted as a percentage.

        Methods can build off each other. This method uses both `current` and
        `max` to return a percentage, saving having to repeat this code in
        various UI elements.

        Returns:
            Health (str): Characters current health as percentage of max.

        Returned if:
            >obj.example.percent
            "50%"
        """
        return "{}%".format(int(self.current * 100.0 / self.max))

    @property
    def team(self):
        """
        Returns current team members.

        This method simply returns the attribute itself.

        Returns:
            team (list): List of objects current team members.

        Returned if:
            >obj.example.team
            [Tweedle-Dee, Tweedle-Dum]
        """
        return self.obj.db.party

    @property
    def alive(self):
        """
        Returns current living team members.

        This method returns the attribute filtered in some way. In this case,
        team members are expected to have an instance of the handler. If their
        current health is 0 then `current` will result in False and so this
        method will only return members who are alive. If `alive` is false
        then all members of your team are dead.

        Returns:
            team (list): List of objects current living team members.

        Returned if:
            >obj.example.alive
            [Tweedle-Dee, Tweedle-Dum]
        """
        return [member for member in self.team if member.example.current]

    """
    Methods.
    Methods are functions which are generally called with arguments and are
    expected to do something, or facilitate some change. These functions and
    their names are not hardcoded and can provide any functionality.

    Methods are called and executed (obj.example.add(<object>)).
    """

    def heal(self, value):
        """
        Support addition between between health and int, capping at max health.

        Returns:
            health (int): Current health after addition.

        Returned if:
            >obj.example.heal(5)
            15
        """
        if isinstance(value, int):
            if (self.current + value) > self.max:
                self.obj.db.health = self.max
                self.obj.msg("You're back to full health!")
                return self.current
            else:
                self.obj.db.health += value
                return self.current
        else:
            msg = '`heal()` method attached to `{}` requires `int` value.'
            raise ExampleException(msg.format(self.obj))

    def dmg(self, value):
        """
        Support subtraction between health and int, capping at 0

        Returns:
            health (int): Current health after subtraction.

        Returned if:
            >obj.example.damage(5)
            10
        """
        if isinstance(value, int):
            if (self.current - value) < 0:
                self.obj.db.health = 0
                self.obj.death()
                return self.current
            else:
                self.obj.db.health -= value
                return self.current
        else:
            msg = '`dmg()` method attached to `{}` requires `int` value.'
            raise ExampleException(msg.format(self.obj))

    def add(self, member):
        """
        Add member to team.

        Returns:
            True (Boolean): Member was added to team successfully.

        Returned if:
            obj.example.add(<Object>)
        """
        # member.on_join_team()
        self.obj.db.party.append(member)

    def remove(self, member):
        """
        Remove member from team.

        Returns:
            True (Boolean): Member was removed from team successfully.

        Returned if:
            obj.party.remove(<Object>)
        """
        if member in self.team:
            self.obj.db.party.remove(member)
            return True
        else:
            return False

    def swap(self, member1, member2):
        """
        Swap members positions within the team.

        Returns:
            Nil.

        Returned if:
            obj.party.swap(<Pokemon>, <Pokemon>)
        """
        party = self.team
        member1, member2 = party.index(member1), party.index(member2)
        party[member2], party[member1] = party[member1], party[member2]

    """
    Special Methods.
    A class can implement certain operations that are invoked by special
    syntax (such as arithmetic operations or subscripting and slicing) by
    defining methods with special names. This is Python’s approach to operator
    overloading, allowing classes to define their own behavior with respect to
    language operators. There are numerous but this will list some of the most
    common for a handler.

    These methods and their names ARE hardcoded and are expected to provide
    specific functionality. For more information see:
    https://docs.python.org/2/reference/datamodel.html#object.__iter__
    """

    def __int__(self):
        """
        Returns current health as int.

        Returns:
            current health (int): Objects current health.

        Returned if:
            >int(obj.example)
            10
        """
        return ', '.join(member.key for member in self.current)

    def __str__(self):
        """
        Returns current team as string.

        Returns:
            team (str): List of current team members in party.

        Returned if:
            >str(obj.example)
            "Tweedle-Dee, Tweedle-Dum"
        """
        return ', '.join(member.key for member in self.current)

    def __len__(self):
        """
        Returns current team length.

        Returns:
            length (int): Number of members in objects team.

        Returned if:
            >len(obj.example)
            2
        """
        return len(self.team)

    def __iter__(self):
        """
        Iterates through team.

        Returns:
            members (<Object>): Values of team members iterated through.

        Returned if:
            >for x in obj.example
        """
        return self.team.__iter__()

    def __getitem__(self, value):
        """
        Select member by list key (int).

        Returns:
            member (<Object>): Value of team members at specific position.

        Returned if:
            >obj.example[1]
        """
        return self.team.__getitem__(value)

    def __add__(self, value):
        """
        Support addition between between health and int, capping at max health.

        Returns:
            health (int): Current health after addition.

        Returned if:
            >obj.example += 5
            15
        """
        if isinstance(value, int):
            if (self.current + value) > self.max:
                self.obj.db.health = self.max
                self.obj.msg("You're back to full health!")
                return self.current
            else:
                self.obj.db.health += value
                return self.current
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __sub__(self, value):
        """
        Support subtraction between health and int, capping at 0

        Returns:
            health (int): Current health after subtraction.

        Returned if:
            >obj.example -= 5
            10
        """
        if isinstance(value, int):
            if (self.current - value) < 0:
                self.obj.db.health = 0
                self.obj.death()
                return self.current
            else:
                self.obj.db.health -= value
                return self.current
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __mul__(self, value):
        """
        Support multiplication between health and int.

        Returns:
            health (int): Current health after multiplication.

        Returned if:
            >obj.heatlh *= 2
            20
        """
        if isinstance(value, int):
            if (self.current * value) > self.max:
                self.obj.db.health = self.max
                self.obj.msg("You're back to full health!")
                return self.current
            else:
                self.obj.db.health *= value
                return self.current
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __floordiv__(self, value):
        """
        Support division between health and int.

        Returns:
            health (int): Current health after division.

        Returned if:
            obj.heatlh /= 5
        """
        if isinstance(value, int):
            if (self.current // value) < 0:
                self.obj.db.health = 0
                self.obj.death()
                return self.current
            else:
                self.obj.db.health /= value
                return self.current
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __nonzero__(self):
        """
        Support Boolean comparison for health.

        Returns:
            alive (Bool): True if health != 0.

        Returned if:
            >if obj.example
            True
        """
        return bool(self.current)

    def __eq__(self, value):
        """
        Support equality comparison for health.

        Returns:
            Equal (Bool): True if equal, False if not.

        Returned if:
            >obj.example == 10
            True
        """
        if isinstance(value, int):
            return self.current == value
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __ne__(self, value):
        """
        Support non-equality comparison for health.

        Returns:
            Not Equal (Bool): True if not equal, False if equal.

        Returned if:
            >obj.example != 5
            True
        """
        if isinstance(value, int):
            return self.current != value
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __lt__(self, value):
        """
        Support less than comparison for heatlh.

        Returns:
            Less than (Bool): True if less than, False if not.

        Returned if:
            >obj.example < 10
            False
        """
        if isinstance(value, int):
            return self.current < value
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __le__(self, value):
        """
        Support less than or equal to comparison for party length.

        Returns:
            Less than or Equal (Bool): True if less than or equal to, False
                                       if not.

        Returned if:
            obj.example <= 10
            True
        """
        if isinstance(value, int):
            return self.current <= value
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __gt__(self, value):
        """
        Support greater than comparison for party length.

        Returns:
            Greater than (Bool): True if greater than, False if not.

        Returned if:
            >obj.example > 10
            False
        """
        if isinstance(value, int):
            return self.current > value
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented

    def __ge__(self, value):
        """
        Support greater than or equal to comparison for party length.

        Returns:
            Greater than or Equal(Bool): True if greater than or equal, False
                                         if not.

        Returned if:
            >obj.example >= 5
            True
        """
        if isinstance(value, int):
            return self.current >= value
        else:
            # Refers to value's class methods to handle interaction.
            return NotImplemented
