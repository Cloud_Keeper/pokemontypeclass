"""
Characters

Characters are (by default) Objects setup to be puppeted by Players.
They are what you "see" in game. The Character class in this module
is setup to be the "default" character type created by the default
creation commands.

"""
from evennia import utils
from typeclasses.objects import Object
from typeclasses.objects_pokemon import Pokemon
from world import rules
from world.itemlist import ITEMLIST
import random


class Item(Object):
    """
    Base Item
    """
    def return_appearance(self, looker):
        """
        This formats a description. It is the hook a 'look' command
        should call.

        Args:
            looker (Object): Object doing the looking.
        """
        if not looker:
            return ""
        # Prepare description string.
        string = "|c%s|n\n" % self.get_display_name(looker)
        desc = self.catalogue("ldesc")
        if desc:
            string += "%s" % desc
        return string

    def catalogue(self, value):
        """
        Perform a search of the catalogue for information of this Item.

        Args:
            value (str): Search value. Will be matched against CATALOGUE[value]

        Returns:
            match (None, str, list or dict): Will return the matched value
                or None.

        """

        return ITEMLIST[self.key].get(value, None)


class Pokeball(Item):
    """
    use pokeball = target
    # TODO
    drop <pokemon> # creates an occupied pokeball object
    picking up a occupied pokeball object adds the pokemon to your party or
    sends it to the box.

    """
    def use_object(self, caller, target, quiet=False):
        """
        Method usually called by the use command. Handles the objects effect.
        Must always receive a caller and target although ignores args it
        doesn't need.
        """
        def calculate_catch(count):
            """

            """
            value = random.randint(0, 65535)
            if value >= wobble_value:
                caller.location.msg_contents(target.key + " broke free!")
                caller.msg(fail_msg[count])
                return
            else:
                if count == 3:
                    successful_catch()
                    return
                else:
                    caller.location.msg_contents(random.choice(shake_msg).
                                                 format(self.key))
                    utils.delay(3, calculate_catch, count+1)

        def successful_catch():
            """

            """
            caller.party.add(target)
            target.location = None
            target.db.trainer = caller
            target.db.pokeball = self.key
            target.db.owner.insert(0, caller)

            caller.msg("You caught %s." % target.name)
            caller.location.msg_contents("%s caught %s." % (caller.name,
                                                            target.name),
                                         exclude=caller)
            self.delete()

        # If no target, drop the Pokeball.
        if not target:
            self.move_to(caller.location, quiet=True)
            caller.location.msg_contents(caller.key + " throws a " + self.key +
                                         " on to the ground.",
                                         exclude=caller)
            caller.msg("You throw a "+self.key+
                       " to the ground. You'll have to pick that up.")
            caller.msg("Use "+self.key+" on what?")
            caller.msg("Usage: 'use "+self.key+" = <pokemon>")
            return

        # Game Messages
        caller.msg("You throw a "+self.key+" at "+target.key)
        caller.location.msg_contents(caller.key + " throws a " + self.key +
                                     " at " + target.key, exclude=caller)
        caller.location.msg_contents("The "+self.key+" contacts "+target.key +
                                     " and opens in a flash of light.")

        # Fail if target not Pokemon or Pokemon has owner.
        if not isinstance(target, Pokemon) or target.db.trainer:
            caller.location.msg_contents("The "+self.key+" fails!")
            caller.msg("You return the "+self.key+" to your inventory.")
            return

        # Calculate catch.
        if self.key in ["Masterball"]:
            successful_catch()
            return

        catch_rate = rules.calculate_catchrate(target.health.current,
                                               target.health.max,
                                               target.pokedex("catch_rate"),
                                               self.catalogue("bonus"), 1)
        if catch_rate >= 255:
            successful_catch()
            return

        wobble_value = rules.calculate_wobblerate(catch_rate)

        shake_msg = ["The {} shakes violently!",
                     "The {} wobbles from side to side!",
                     "The {} shudders harshly!",
                     "The {} strains to keep closed!"]

        fail_msg = ["Oh no! The Pokemon broke free!",
                    "Aww! It appeared to be caught!",
                    "Aargh! Almost had it!",
                    "Shoot! It was so close, too!"]

        utils.delay(3, calculate_catch, 0)
