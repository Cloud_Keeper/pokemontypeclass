"""
Apartment Room

Rooms are simple containers that has no location of their own.

Character values:
        self.db.property_owned = {}  # {<location>: <room>}
        self.db.property_accessible = {}  # {<location>: [<room>, <room>]}

"""

from typeclasses.rooms import Room
from typeclasses import exits, characters_trainers
from evennia import CmdSet
from evennia.utils.utils import class_from_module
from django.conf import settings
from evennia.utils import create
from evennia.commands.default import general
from commands import command_trainers

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)


class CmdGet(general.CmdGet):
    """Replaces the default get command so it respects room permissions."""
    key = "get"
    aliases = ["grab"]

    def func(self):
        if self.obj.has_access(self.caller, "get"):
            super(CmdGet, self).func()
        else:
            self.caller.msg("You do not have permission to take that action.")


class CmdPermissions(COMMAND_DEFAULT_CLASS):
    """Permission"""
    key = "permissions"
    aliases = ["permission", "perm"]

    def func(self):
        # If not args, display permissions list
        if not self.args:
            # Display owner
            self.caller.msg("Owner:")
            self.caller.msg(str(self.obj.db.owner))
            self.caller.msg("\n")

            # Get list of permissions split by level.
            permdict = {}
            for key in self.obj.db.levels:
                permdict[key] = []
            for key, val in self.obj.db.permissions.iteritems():
                if val not in permdict:
                    permdict[val] = [key.key]
                else:
                    permdict[val].append(key.key)
            # Display characters at each level
            self.caller.msg("Levels:")
            for key in permdict:
                self.caller.msg(key + "(" + str(self.obj.db.levels.get(key)) + ")")
                self.caller.msg("\n")
                self.caller.msg(permdict[key])
                self.caller.msg("\n")
            return

        # Permission functions:

        # Handle add/edit switch. >permissions/add builder = access, build
        if [x for x in self.switches if x in ["new", "edit"]]:
            self.obj.db.levels[self.lhs] = self.rhslist
            self.caller.msg("Permission level: '" + self.lhs + ": " + str(self.rhslist) + "' has been added to permissions.")
            return

        # Handle delete switch. >permissions/delete builder
        if [x for x in self.switches if x in ["del", "delete"]]:
            if self.args not in self.obj.db.levels:
                self.caller.msg("Room does not have permission level '{}'.".format(self.args))
                return
            del self.obj.db.levels[self.args]
            self.caller.msg("Permission level '{}' has been deleted.".format(self.args))
            return

        # Player functions:

        target = self.caller.search(self.lhs,
                                    typeclass=characters_trainers.Trainer,
                                    global_search=True,
                                    nofound_string="No player by name '{}'.".format(self.lhs))
        if not target:
            return

        # Handle add switch >permissions/add player =

        # Handle ban switch. >permissions/remove player
        if [x for x in self.switches if x in ["remove", "ban"]]:
            if target in self.obj.db.permissions:
                self.obj.remove_access(target)
                self.caller.msg("Player '{}' has had access removed.".format(target.key))
                return
            else:
                self.caller.msg("Player '{}' has no recorded permissions.".format(target.key))
                return
        else:
            self.obj.add_access(target, access=self.rhs if self.rhs else "access")
            self.caller.msg("Player '{}' has been granted permission level {}.".format(target.key, self.rhs if self.rhs else "access"))
            return


class CmdKick(COMMAND_DEFAULT_CLASS):
    """Kick"""
    key = "kick"

    def func(self):
        if not self.caller == self.obj.db.owner:
            self.caller.msg("Only the owner can kick people.")

        target = self.caller.search(self.args,
                                    nofound_string="Could not find {}.".format(
                                        self.args))
        if not target:
            return

        self.obj.kick(target)


class ApartmentCmdSet(CmdSet):
    key = "ApartmentCmdSet"
    priority = 1
    def at_cmdset_creation(self):
        self.add(CmdGet())
        self.add(CmdPermissions())
        self.add(CmdKick())


class Apartment(Room):
    """
    Rooms are like any Object, except their location is None
    (which is default). They also use basetype_setup() to
    add locks so they cannot be puppeted or picked up.
    (to change that, use at_object_creation instead)

    See examples/object.py for a list of
    properties and methods available on all Objects.
    """
    def at_object_creation(self):
        """
        Called only once, when object is first created
        """
        super(Apartment, self).at_object_creation()
        # Where the apartment is located.
        self.db.location = None

        # Mandatory exit.
        create.create_object(exits.ApartmentExit, key="Exit", aliases=["e"],
                     location=self)

        # Attributes covering rights and permissions.
        self.db.owner = None  # <Character> or <Guild script>
        self.db.permissions = {}  # {<Character>: "Permission level"}
        self.db.levels = {
            "access": ["access"],
            "admin": ["full"]
        }

        # Room specific commands
        if not self.cmdset.has(ApartmentCmdSet):
            self.cmdset.add(ApartmentCmdSet, permanent=True)

    def add_access(self, target, access="access"):
        """Add person to permissions with the given access level"""

        # Add room reference to characters apartment list.
        if self.db.location not in target.db.property_accessible:
            target.db.property_accessible[self.db.location] = [self]
        elif self not in target.db.property_accessible[self.db.location]:
            target.db.property_accessible[self.db.location].append(self)

        # Add character reference to room with permission.
        self.db.permissions[target] = access if access else "access"

        return True

    def remove_access(self, target):
        """Remove persons permissions"""

        del self.db.permissions[target]
        target.db.property_accessible.remove(self)

    def has_access(self, target, access=None):
        """Returns True if has any access, or specfic access if specified"""
        if target == self.db.owner:
            return True

        if target not in self.db.permissions:
            return False

        if not access:
            return True

        if access in self.db.permissions[target]:
            return True

    def delete(self):
        """Removes access to remove from linked characters."""
        del self.db.owner.db.property_owned[self.db.location]
        for char in self.db.permissions:
            self.remove_access(char)
        super(Apartment, self).delete()

    def kick(self, target):
        """Kick person"""
        if target in self.contents:
            target.move_to(self.db.location)
            target.msg("You have been removed from the room.")
