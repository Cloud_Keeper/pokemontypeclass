# -*- coding: utf-8 -*-
"""
Commands

Commands describe the input the player can do to the game.

Future Ben: Whilst commands should only be middle men for methods that do
all the work, we should assume that everything that reaches the method is
correct. All the checking should be done in the command.

first stage - send inputs out to everyone. stage doesn't start unless everyone
                yes or aborts
second stage - everyone chooses pokemon until conditions are met.
third stage - battle

"""
from django.conf import settings
from evennia import Command
from evennia.utils import evtable, evmenu
from evennia import create_script
from typeclasses.characters_trainers import Trainer
from typeclasses.objects_pokemon import Pokemon
from evennia import CmdSet
from evennia.utils.utils import class_from_module

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)

#########################
# Always Available Cmdset
#########################


class CombatCmdSet(CmdSet):
    """
    The battle related commands available to all trainers.
    """
    key = "combat_cmdset"
    priority = 1

    def at_cmdset_creation(self):
        self.add(CmdBattle())


def battle_node_formatter(nodetext, optionstext, caller=None):
    """
    A minimalistic node formatter used by the Battle Builder Menu.
    """
    return nodetext + "\n" + optionstext


def battle_options_formattter(optionlist, caller=None):
    """
    A minimalistic options formatter used by the Battle Builder Menu.
    """
    if not optionlist:
        return ""

    return "You see: " + ", ".join([key for key, msg in optionlist])


def battle_builder_node(caller, input_string):
    """
    The advanced battle building menu called by the Battle Command.

    Args:
        caller:
        input_string:

    Returns:

    """
    # Set up variables
    menu = caller.ndb._menutree

    # Parse input
    # party/add *
    # party/remove *

    # Draw table.
    table = evtable.EvTable("Battle Builder", border="table")
    table.reformat(corner_char="◓", border_char="═",
                   border_left_char=" ", border_right_char=" ")

    for i in xrange(len(menu.teams)):
        table.add_row("Team " + str(i + 1) + ": " + ", ".join(str(trainer) for trainer in menu.teams[i]) + ".")
    table.add_row("Rules:")
    table.add_row("Party Size = " + str(menu.party_number))
    table.add_row("Combatants = " + str(menu.field_number))

    # Set up return options.
    text = table
    options = [{"key": "_default",
                "goto": "battle_builder_node"}]

    return text, options


class CmdBattle(COMMAND_DEFAULT_CLASS):
    """
    Initiates battle

    For a standard battle:
        battle <trainer>

    Advanced Usage:
        battle[/switch] <trainer>:<trainer>, <trainer>:...]

    For 2 teams of 2 trainers, 2 pokemon each with 2 reserves.
        battle/f2/t4 <trainer>:<trainer>, <trainer>:<trainer>, <trainer>
        battle/f2/t4 :<trainer>:<trainer>

    switch:
        /f# : # Number of pokemon to field. Max 6 pokemon (1 by defualt).
        /p# : # Number of pokemon in party. Minimum 1 pokemon (6 by default).
        /builder : Starts battle builder evmenu
        # Can double up switches "battle/5/3 trainer"
        Consider for pokemon out at a time and team size.

    # builder arg. Battle starts if not set, otherwise builder starts.
    # bets
    # Individual field number and party limit.
    # if error getting field or party number then start builder (set arg).
    """
    key = "battle"
    aliases = ["challenge", "fight"]
    help_category = "General"

    def func(self):
        """
        We want to pass 3 values to the Combat Handler:
            rules [dict]: The parameters of the battle.
            teams [list]: The trainers separated into teams.
        """
        caller = self.caller

        if not self.args:
            caller.msg("Battle whom?")
            return

        # RULES
        rules = {"party_size": 6,
                 "field_size": 1}
        for value in self.switches:
            # Use given value if within limits or use default values.
            rules["party_size"] = int(value[1]) if (value[0] == "p" and int(value[1]) > 0 and int(value[1]) < 7) else rules["party_size"]
            rules["field_size"] = int(value[1]) if (value[0] == "f" and int(value[1]) > 0 and int(value[1]) <= rules["party_size"]) else rules["field_size"]

        # TEAMS.
        trainers = []

        # Add caller to str: caller.key + "," + "trainer1:trainer2, trainer3"
        teams = caller.key + ", " + self.args

        # Split into teams: ["caller, trainer1","trainer2, trainer3"]
        teams = teams.split(":")

        for x in xrange(len(teams)):
            # Split again: [["caller", "trainer1"],["trainer2", "trainer3"]]
            teams[x] = teams[x].split(",")

            for y in xrange(len(teams[x])):
                # Find trainer object for each string or command fails.
                obj = caller.search(teams[x][y].strip(), typeclass=Trainer)

                if not obj:
                    caller.msg(
                        teams[x][y] + " could not be found. Battle aborted.")
                    return

                if obj.ndb.combat_handler:
                    caller.msg(
                        teams[x][y] + " already in battle. Battle aborted.")
                    return

                if obj in trainers:
                    caller.msg(
                        teams[x][y] + " mentioned twice. Battle aborted.")
                    return
                # Result [[<caller>, <trainer1>],[<trainer2>, <trainer3>]]
                teams[x][y] = obj
                trainers.append(obj)

        # Single target commands will return one team = [[<caller>, <bob>]]
        # Convert to a 1 v 1: [[<caller>], [<trainer>]]
        if len(teams) == 1:
            teams.append([teams[0].pop(0)])

        # Start Battle if able and builder not specifically called.
        if len(teams) > 1 and "builder" not in self.switches:
            handler = create_script("typeclasses.handler_combat.CombatHandler")
            handler.init_battle(rules, teams, {}, "Challenge")
            return

        # Start Builder with values
        # TO DO
        return

#########################
# Challenge Stage Cmdset
#########################


class ChallengeCmdSet(CmdSet):
    """

    """
    key = "challengecmdset"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdAccept())
        self.add(CmdDecline())


class CmdAccept(COMMAND_DEFAULT_CLASS):
    """

    """
    key = "yes"
    aliases = ["y", "accept"]
    help_category = "General"

    def func(self):
        """

        """
        self.caller.msg("You have accepted the invitation.")
        self.caller.ndb.combat_handler.challenge_callback(self.caller, True)


class CmdDecline(COMMAND_DEFAULT_CLASS):
    """

    """
    key = "no"
    aliases = ["n", "decline", "run"]
    help_category = "General"

    def func(self):
        """

        """
        self.caller.msg("You have declined the invitation.")
        self.caller.ndb.combat_handler.challenge_callback(self.caller, False)

#########################
# Action Stage Cmdset
#########################


class ActionCmdSet(CmdSet):
    key = "actioncmdset"
    mergetype = "Merge"
    priority = 10
    no_exits = True

    def at_cmdset_creation(self):
        self.add(CmdAbort())
        self.add(CmdRun())
        self.add(CmdSelect())
        self.add(CmdSwap())
        self.add(CmdDebug())


class CmdDebug(COMMAND_DEFAULT_CLASS):
    """
    Usage:
      run

    """
    key = "debug"
    help_category = "General"

    def func(self):
        """
        Handle command
        """
        self.caller.ndb.combat_handler.debug(self.caller)


class CmdAbort(COMMAND_DEFAULT_CLASS):
    """
    Usage:
      run

    """
    key = "abort"
    help_category = "General"

    def func(self):
        """
        Handle command
        """
        for i in self.caller.ndb.combat_handler.db.trainers:
            i.msg("Battle has been aborted.")
        self.caller.ndb.combat_handler.stop()


class CmdRun(COMMAND_DEFAULT_CLASS):
    """
    Usage:
      run

    """
    key = "run"
    help_category = "General"

    def func(self):
        """
        # TO DO
        # Check if against trainer
        # Check all pokemon in all field postions
        # Set all pokemons actions to run.
        """
        caller = self.caller
        args = self.lhs
        battle = caller.ndb.combat_handler
        rplist = self.rhs.split(":") if self.rhs else None

        party = battle.db.trainers[caller]
        # TODO What if you don't have any pokemon left to fill spot?
        if not all(party):
            self.caller.msg("You must have all positions full to run.")
            return

        self.caller.msg("Your Pokemon and you will run when you are able!")
        for member in party:
            battle.action_callback("Run", None, caller, member, None, rplist)


class CmdSelect(COMMAND_DEFAULT_CLASS):
    """
    Usage:
      select <pokemon> = [roleplay]

    If pokemon isn't out of pokeball then it gets cast when everyone has
    selected their Pokemon, along with any RP.

    """
    key = "Select"
    help_category = "General"

    def func(self):
        """
        Handle command
        """
        # Prepare values.
        caller = self.caller
        battle = caller.ndb.combat_handler
        args = self.args
        rplist = self.rhs.split(":") if self.rhs else None

        if not args:
            self.caller.msg("Usage: select <pokemon> = [roleplay]")
            return

        # args list has at least one value. Identify Pokemon or return.
        no_find = "'{}' is not in your party. Select a Pokemon."
        args = caller.search(args, candidates=caller.party.list,
                             nofound_string=no_find.format(args))
        if not args:
            return

        # If space on field: enter Pokemon in battle, else: return.
        if not all(battle.db.trainers[caller]):
            battle.action_callback("Select", None, caller, args,
                                   None, rplist)
            caller.msg("You have selected {}".format(args.key))
            return
        else:
            caller.msg("Swap with Pokemon in battle instead.")
            return


class CmdSwap(COMMAND_DEFAULT_CLASS):
    """
    Usage:
      swap <swapwith> <swapto> = [roleplay]

    If pokemon isn't out of pokeball then it gets cast when everyone has
    selected their Pokemon, along with any RP.

    """
    key = "Swap"
    help_category = "General"

    def func(self):
        """
        Handle command
        """
        # Prepare values.
        caller = self.caller
        battle = caller.ndb.combat_handler
        args = self.lhs.split()
        rplist = self.rhs.split(":") if self.rhs else None

        if not args or len(args) != 2:
            self.caller.msg("Usage: select <pokemon> [<target>] = [roleplay]")
            return

        # Identify target or return.
        no_find = "'{}' is not fielded by you in battle. Select a Pokemon"
        args[0] = caller.search(args[0],
                                candidates=battle.db.trainers[caller],
                                nofound_string=no_find.format(args[0]))
        if not args[0]:
            return

        # Identify Pokemon or return.
        no_find = "'{}' is not in your party. Select a Pokemon."
        args[1] = caller.search(args[1],
                                candidates=caller.party.list,
                                nofound_string=no_find.format(args[1]))
        if not args[1]:
            return

        # Enter Pokemon into battle in exchange with target.
        battle.action_callback("Swap", None, caller, args[0],
                               args[1], rplist)
        caller.msg("You have selected {} to swap with {}".format(
            args[1].key, args[0]))
        return


class PokemonCommand(COMMAND_DEFAULT_CLASS):
    """
    Pokemon [use] <Action> [on] <Target> [= Message]
    """
    obj = None

    def func(self):
        """
        move set.
        """
        caller = self.caller
        pokemondict = caller.ndb.combat_handler.db.pokemon
        args = self.lhs
        rplist = self.rhs.split(":") if self.rhs else None

        if not args:
            caller.msg("Tell Pokemon to do what?")
            return

        text = args.lower().split()
        for x in args.lower().split():
            if x in ["use", "on"]:
                text.remove(x)

        if len(text) > 2:
            caller.msg("Usage: Pokemon [use] <Action> [on] <Target>")
            return

        # Handle Target
        target = None
        if len(text) == 2:
            for pokemon in pokemondict.keys():
                if text[1].capitalize() == pokemon.key:
                    target = pokemon
                    break

            if not target:
                caller.msg("Invalid target entered")
                return
            else:
                # Check for teams
                self.obj.ndb.target = target

        if not self.obj.ndb.target:
            caller.msg("Must specify a target.")
            return

        # Handle Action
        if text[0].capitalize() not in self.obj.moves.list:
            caller.msg("Invalid action entered")
            return

        self.caller.msg(self.obj.key + " will do a " + text[0].capitalize() +
                        " attack.")
        self.caller.ndb.combat_handler.action_callback("Fight",
                                                  text[0].capitalize(),
                                                  caller,
                                                  self.obj,
                                                  self.obj.ndb.target, rplist)
