"""
Property commands.
"""
from evennia.utils.utils import class_from_module
from typeclasses.characters_trainers import Trainer
from django.conf import settings
from evennia import CmdSet

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)

# Used by CmdUse
_USE_ERRMSG = "You don't have '{}' in your inventory."
_TGT_ERRMSG = "'{}' could not be located."


class PropertiesCmdSet(CmdSet):
    """CmdSet for Builder commands."""
    key = "trainercmdset"
    priority = 1

    def at_cmdset_creation(self):
        # Pokemon Building
        self.add(CmdProperty())


class CmdProperty(COMMAND_DEFAULT_CLASS):
    """

    """
    key = "property"
    aliases = ["properties", "p", "prop"]
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """ """

        self.caller.msg("Property you own:")
        self.caller.msg(str(self.caller.db.property_owned))
        self.caller.msg("\n")
        self.caller.msg("Property accessible to you:")
        self.caller.msg(str(self.caller.db.property_accessible))
