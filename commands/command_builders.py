"""
Builder commands.
"""
from evennia.utils.utils import class_from_module, to_str
from django.conf import settings
from ast import literal_eval
from utils.utils_pokemon import create_pokemon
from utils.utils_items import create_item
from evennia import CmdSet

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)


class BuildersCmdSet(CmdSet):
    """CmdSet for Builder commands."""
    key = "buildercmdset"
    priority = 1

    def at_cmdset_creation(self):
        # Pokemon Building
        self.add(CmdCreatePokemon())
        self.add(CmdCreateItem())


class CmdCreatePokemon(COMMAND_DEFAULT_CLASS):
    """
    Create a Pokemon. Fairly forgiving. As long as Pokemon name is right
    you'll get a pokemon on the other side.
    """

    key = "@createpokemon"
    aliases = ["@cp"]
    locks = "cmd:perm(spawn) or perm(Builders)"
    help_category = "Building"

    def func(self):

        # If no arguments provided, provide instructions.
        if not self.args:
            self.caller.msg("Usage: @createpokemon Pokemon OR {'key':'val', ..}")
            return

        # Convert arguments to a Python Construct or a string.
        try:
            args = literal_eval(self.args)
        except (SyntaxError, ValueError):
            args = to_str(self.args)

        # If string: create pokemon from just string.
        if isinstance(args, basestring):

            try:
                poke = create_pokemon(args, location=self.caller.location)
                if poke:
                    self.caller.msg(
                        str(poke.key) + " " + str(poke.dbref) + " spawned.")

            except AttributeError as err:
                self.caller.msg("Command failed: " + str(err))

        # If not string or dict then abort.
        elif not isinstance(args, dict):
            self.caller.msg("Usage: @createpokemon Pokemon OR {'key':'val', ..}")
            return

        else:
            # Left with dictionary. Create pokemon with values given.
            default_values = {"key": None,
                              "location": self.caller.location,
                              "level": 1,
                              "iv": None,
                              "ev": None,
                              "gender": None,
                              "trainer": None,
                              "owner": None}

            # Replace defaults with given values.
            for key in args:
                if key in default_values:
                    default_values[key] = args[key]
                else:
                    # If no match, inform user and proceed without.
                    string = " not valid argument. Proceedings without."
                    self.caller.msg(str(key) + ":" + str(args[key]) + string)

            # Create pokemon from dictionary

            try:
                poke = create_pokemon(default_values["key"],
                                      location=default_values["location"],
                                      level=default_values["level"],
                                      iv=default_values["iv"],
                                      ev=default_values["ev"],
                                      gender=default_values["gender"],
                                      trainer=default_values["trainer"],
                                      owner=default_values["owner"])
                if poke:
                    self.caller.msg(
                        str(poke.key) + " " + str(poke.dbref) + " spawned.")

            except AttributeError as err:
                self.caller.msg("Command failed: "+str(err))


class CmdCreateItem(COMMAND_DEFAULT_CLASS):
    """
    Create an Item.
    """

    key = "@createitem"
    aliases = ["@ci"]
    locks = "cmd:perm(spawn) or perm(Builders)"
    help_category = "Building"

    def func(self):

        # If no arguments provided, provide instructions.
        if not self.args:
            self.caller.msg("Usage: @createitem <item>")
            return

        if "drop" in self.switches:
            location = self.caller.location
        else:
            location = self.caller

        item = create_item(key=self.args, location=location)

        if not item:
            self.caller.msg("Command failed: Valid Item not given.")
            return

        self.caller.msg(item.key + " was created.")
