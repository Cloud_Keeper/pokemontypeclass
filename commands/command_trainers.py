"""
Builder commands.
"""
from evennia.utils.utils import class_from_module
from typeclasses.characters_trainers import Trainer
from django.conf import settings
from evennia import CmdSet

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)

# Used by CmdUse
_USE_ERRMSG = "You don't have '{}' in your inventory."
_TGT_ERRMSG = "'{}' could not be located."


class TrainerCmdSet(CmdSet):
    """CmdSet for Builder commands."""
    key = "trainercmdset"
    priority = 1

    def at_cmdset_creation(self):
        # Pokemon Building
        self.add(CmdTalk())
        self.add(CmdUse())
        self.add(CmdChoose())
        self.add(CmdReturn())


class CmdTalk(COMMAND_DEFAULT_CLASS):
    """
    Talk to a NPC. Triggers the corresponding objects at_talk hook.

    Usage:
        talk <obj> =['say message][:pose message][:'say message]
    """
    key = "talk"
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """Talk to an Character."""

        # Set up function variables.
        caller = self.caller
        rplist = self.rhs.split(":") if self.rhs else None

        # Find and confirm suitability of target.
        if not self.args:
            caller.msg("Talk to whom?")
            return

        target = caller.search(self.args,
                               typeclass=Trainer,
                               nofound_string="You cannot talk to {}.".format(self.args))
        if not target:
            return

        # Handle roleplay entries.
        if rplist:
            for text in rplist:
                if text[0] in ["'"]:
                    caller.execute_cmd("Say " + text[1:])
                else:
                    caller.execute_cmd("Pose " + text)

        # Call use_object hook on object.
        target.at_talk(caller)


class CmdUse(COMMAND_DEFAULT_CLASS):
    """
    Use an object

    Usage:
        use <obj> [on <target>] =['say message][:pose message][:'say message]

    The command takes inspiration by the use command and hooks in the Muddery
    Evennia project. The command is used on items in the characters inventory.
    The command simply sanity checks arguments before calling the objects
    use_object() function. A string is returned by the function which is then
    passed on to the character.

    ** at this stage only in your inventory.
    """
    key = "use"
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """Use an object."""

        # Set up function variables.
        caller = self.caller
        args = self.lhs.split("on")
        obj = args[0].strip() if len(args) >= 1 else None
        target = args[1].strip() if len(args) > 1 else None
        rplist = self.rhs.split(":") if self.rhs else None

        # Find and confirm suitability of obj.
        if not obj:
            caller.msg("Use what?")
            return

        obj = caller.search(obj, nofound_string=_USE_ERRMSG.format(obj))
        if not obj:
            return

        if not getattr(obj, "use_object", None):
            caller.msg("You cannot use this object.")
            return

        # If target given: find target.
        if target:
            target = caller.search(target,
                                   nofound_string=_TGT_ERRMSG.format(target))
            if not target:
                return

        # Handle roleplay entries.
        if rplist:
            for text in rplist:
                if text[0] in ["'"]:
                    caller.execute_cmd("Say " + text[1:])
                else:
                    caller.execute_cmd("Pose " + text)

        # Call use_object hook on object.
        obj.use_object(caller, target)


class CmdChoose(COMMAND_DEFAULT_CLASS):
    """
    Cast Pokemon in your party to your location.

    Usage:
        choose <Pokemon> =['say message][:pose message][:'say message]

    """

    key = "choose"
    aliases = ["go", "I choose you"]
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """Choose a Pokemon"""

        # Set up function variables
        caller = self.caller
        party = caller.party
        suitabletargets = [x for x in party.list if not x.location]
        target = self.lhs
        rplist = self.rhs.split(":") if self.rhs else None

        # Find and confirm suitability of target.
        if not target:
            self.caller.msg("Select a Pokemon from your Party.\n"
                            "Available Pokemon: " +
                            str([x.key for x in suitabletargets]) + "\n"
                            "Usage: 'choose <Pokemon>'")
            return

        pokemon = caller.search(target, candidates=suitabletargets,
                                nofound_string=("You do not have '{}' in your "
                                                "party.".format(target)))

        if not pokemon:
            return

        # Handle roleplay entries.
        if rplist:
            for text in rplist:
                if text[0] in ["'"]:
                    self.caller.execute_cmd("Say " + text[1:])
                else:
                    self.caller.execute_cmd("Pose " + text)
        else:
            self.caller.execute_cmd("Say Go " + pokemon.key + "!")

        # Cast Pokemon.
        party.cast(pokemon)

class CmdReturn(COMMAND_DEFAULT_CLASS):
    """
    Return Pokemon in your party to it's Pokeball.

    Usage:
        return <Pokemon> =['say message][:pose message][:'say message]

    """

    key = "return"
    aliases = ["recall"]
    locks = "cmd:all()"
    help_category = "General"

    def func(self):
        """Return a Pokemon"""

        # Set up function variables
        caller = self.caller
        party = caller.party
        suitabletargets = [x for x in party.list if x.location == caller.location]
        target = self.lhs
        rplist = self.rhs.split(":") if self.rhs else None

        # Find and confirm suitability of target.
        if not target:
            self.caller.msg("Select a Pokemon from your Party.\n"
                            "Available Pokemon: " +
                            str([x.key for x in suitabletargets]) + "\n"
                            "Usage: 'return <Pokemon>'")
            return

        pokemon = caller.search(target, candidates=suitabletargets,
                                nofound_string=("You do not have '{}' in your "
                                                "party.".format(target)))

        if not pokemon:
            return

        # Handle roleplay entries.
        if rplist:
            for text in rplist:
                if text[0] in ["'"]:
                    self.caller.execute_cmd("Say " + text[1:])
                else:
                    self.caller.execute_cmd("Pose " + text)
        else:
            self.caller.execute_cmd("Say Come back " + pokemon.key + "!")

        # Cast Pokemon.
        party.recall(pokemon)
