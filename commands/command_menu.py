# -*- coding: utf-8 -*-
"""
Builder commands.
"""
from evennia.utils.utils import class_from_module
from typeclasses.characters_trainers import Trainer
from evennia.utils import utils, evform
from world.rules import BADGES
from django.conf import settings
from evennia import CmdSet
import time

COMMAND_DEFAULT_CLASS = class_from_module(settings.COMMAND_DEFAULT_CLASS)


class MenuCmdSet(CmdSet):
    """CmdSet for Builder commands."""
    key = "MenuCmdSet"
    priority = 1

    def at_cmdset_creation(self):
        # Pokemon Building
        self.add(CmdMenu())
        self.add(CmdPartyMenu())
        self.add(CmdTrainerMenu())


class CmdMenu(COMMAND_DEFAULT_CLASS):
    """
    Trainer menu

    Usage:
      menu

    This recreates the view of a Pokemon player pressing the start button. It
    provides a list of the main menu options available to the player.

    """

    key = "menu"
    locks = "cmd:all()"
    aliases = ["m", "start"]
    help_category = "General"

    def func(self):

        sheet = """\
                                ◓═════════◓
                                ║ POKeDEX ║
                                ║ POKeMON ║
                                ║ ITEM    ║
                                ║ TRAINER ║
                                ║ SAVE    ║
                                ║ OPTION  ║
                                ║ EXIT    ║
                                ◓═════════◓\
                """

        self.msg(unicode(sheet))


class CmdPartyMenu(COMMAND_DEFAULT_CLASS):
    """
    Party menu

    Usage:
      pokemon
      party

    This provides the player with their party screen, mirroring the form
    of the party screen in the Pokemon game, displaying the party members
    name, level, and health.

    """

    key = "party"
    locks = "cmd:all()"
    aliases = ["pokemon"]
    help_category = "General"

    def func(self):

        text = {"FORM": """
                  x1xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x2xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x3xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x4xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                  x6xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                 ◓═════════════════════════════════════════◓
                 ║                                         ║
                 ║            Choose a POKEMON             ║
                 ║                                         ║
                 ◓═════════════════════════════════════════◓\
                """}

        # Set up entries.
        line = {"FORM": "\n◓ xAxxxxxxxxxxxxxxxxxxxx :LxBx  xCx / xDx"}

        cells = {}
        index = 1
        for poke in self.caller.party:
            form = evform.EvForm(form=line,
                                 cells={"A": poke.key,
                                        "B": "   "[:-len(str(poke.db.level))]
                                             + str(poke.db.level),
                                        "C": "   "[:-len(str(poke.health))]
                                             + str(poke.health),
                                        "D": "   "[:-len(str(poke.health.max))]
                                             + str(poke.health.max)})
            cells[index] = form
            index += 1

        # Build EvForm from entries.
        form = evform.EvForm(form=text, cells=cells)
        self.msg(unicode(form))


class CmdTrainerMenu(COMMAND_DEFAULT_CLASS):
    """
    Trainer menu

    Usage:
      trainer [trainer]

    This provides the player with their own or another trainers character
    sheet, mirroring the form of the trainer screen in the Pokemon game. It
    displays the Trainers name, money, time played and badges.

    """

    key = "trainer"
    locks = "cmd:all()"
    aliases = ["c", "cha", "character"]
    help_category = "General"

    def func(self):

        text = {"FORM": """
                ┏━━━━━━━━━━━━━━━━━━━━━━━┓
                ┃   NAME/   xxxxxAxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃   MONEY/  ₱xxxxBxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┃   TIME/   xxxxxCxxxxxxxxxxxxxxxxxxxxxxx  ┃
                ┣━━━━━━━━━━━━━━━━━━━━━━━┫
                ┃                 ●BADGES●                 ┃
                ┣━━━━━━━━━━━━━━━━━━━━━━━┫
                ┃   1.        2.        3.         4.      ┃
                ┃       xDx       xEx       xFx        xGx ┃
                ┃   5.        6.        7.         8.      ┃
                ┃       xHx       xIx       xJx        xKx ┃
                ┗━━━━━━━━━━━━━━━━━━━━━━━┛\
                """}

        # Find Trainer target.
        caller = self.caller
        if not self.args:
            trainer = caller
        else:
            trainer = caller.search(self.args, typeclass=Trainer)
            if not trainer:
                caller.msg("Trainer could not be found.")
                return

        # TIME is stored on session. Required for C
        sessions = trainer.sessions.get()
        session = sessions[0] if sessions else None
        play_time = utils.time_format(trainer.db.time_played +
                                      (time.time() - session.conn_time), 2),

        # BADGES is stored on the trainer. Required for D-K
        gyms = trainer.db.badges_kanto

        # Build EvForm from values.
        form = evform.EvForm(form=text,
                             cells={"A": trainer.key,
                                    "B": trainer.db.money,
                                    "C": play_time[0],
                                    "D": BADGES[0] if gyms[0] else "?",
                                    "E": BADGES[1] if gyms[1] else "?",
                                    "F": BADGES[2] if gyms[2] else "?",
                                    "G": BADGES[3] if gyms[3] else "?",
                                    "H": BADGES[4] if gyms[4] else "?",
                                    "I": BADGES[5] if gyms[5] else "?",
                                    "J": BADGES[6] if gyms[6] else "?",
                                    "K": BADGES[7] if gyms[7] else "?"})
        self.msg(unicode(form))
